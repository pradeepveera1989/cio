<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Middleware;

/**
 * Description of ValidationErrorsMIddleware
 *
 * @author Pradeep
 */
class ValidationErrorsMIddleware extends Middleware{
    //put your code here
    
    public function __invoke($request, $response, $next) {
       
        if(!empty($_SESSION['blocked'])){
            
            $this->container->view->getEnvironment()->addGlobal('user_blocked', $_SESSION['blocked']);

            unset($_SESSION['blocked']);            
        }       
        
        if(!empty($_SESSION['failed_login'])){
            
            $this->container->view->getEnvironment()->addGlobal('failed_login', $_SESSION['failed_login']);

            unset($_SESSION['failed_login']);            
        }
        
        if(!empty($_SESSION['validation_errors'])){

            $this->container->view->getEnvironment()->addGlobal('errors', $_SESSION['validation_errors']);

            unset($_SESSION['validation_errors']);
        }  
        
        if(!empty($_SESSION['failed_registration'])){
        
            $this->container->view->getEnvironment()->addGlobal('failed_registration', $_SESSION['failed_registration']);

            unset($_SESSION['failed_registration']);
        }  
               
        if(!empty($_SESSION['message'])){
        
            $this->container->view->getEnvironment()->addGlobal('message', $_SESSION['message']);

            unset($_SESSION['message']);
        }         
                
        $response = $next($request, $response);
        
        return $response;
    }
}
