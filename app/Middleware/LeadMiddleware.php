<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Middleware;

/**
 * Description of LeadMiddleware
 *
 * @author Pradeep
 */
class LeadMiddleware extends Middleware
{
    //put your code here
    public function __invoke($request, $response, $next) {

        // Convert Session to Global variable.
        if($_SESSION['notify_leads']){

            $this->container->view->getEnvironment()->addGlobal('notify_leads', $_SESSION['notify_leads']);
            unset($_SESSION['notify_leads']);
        }

        // Convert Session to Global variable.
        if($_SESSION['error_leads']){

            $this->container->view->getEnvironment()->addGlobal('error_leads', $_SESSION['error_leads']);
            unset($_SESSION['error_leads']);
        }

        $response = $next($request, $response);
        return $response;
    }
}