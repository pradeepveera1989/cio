<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Company
 *
 * @author Pradeep
 */

namespace App\Models;

class Company extends Model
{
    //put your code here
   
    public $company_id;

    
    /**
     * createCompany function
     *
     * @param string $email
     * @return integer
     */
    public function createCompany(string $domain) : int
    {
        $comp_id = 0;
        if (empty($domain)) {
            return $comp_id;
        }
        // Validates with Free domain
        if (!$this->validateFreeDomains($domain)) {
            return $comp_id;
        }
        // Check if Company exists in DB
        $comp_id = $this->getCompanyDetialsByWebsite($domain)['id'];

        if (is_numeric($comp_id) && (int) $comp_id > 0) {
            return $comp_id;
        }
        // Generate new Account id
        $acc_id = $this->getNewAccountNo();
        $update = $this->getUpdateDetials();
        $data = [
            'website' => $domain,
            'account_no' => $acc_id,
            'created' => $update['date'],
            'updated' => $update['date'],
        ];

        $sql = "INSERT INTO `cio.company`(`website`, `account_no`,`created`,`update`) "
                ." VALUES ( :website, :account_no, :created, :updated)";
        $result = $this->excuteQuerySET($sql, $data);

        // Get latest record id from Table
        if (is_bool($result) && $result == true) {
            $comp_id = $this->getLastestRecordFromTable('cio.company', 'id')['id'];
        }

        return $comp_id;
    }

    
    /**
     * getAccountIdByDomain function
     * Retrives the Account id for Company based on Website
     *
     * @param string $domain
     * @return array
     */
    public function getCompanyDetialsByWebsite(string $domain) : array
    {
        $comp_details = [];
        
        if (empty($domain)) {
            return $comp_details;
        }

        $qry = "SELECT * FROM `cio.company` WHERE `website` LIKE  '$domain' ";
        $result = $this->excuteQueryGET($qry)[0];

        if (is_array($result) && (int)$result['id'] > 0) {
            $comp_details = $result;
        }

        return $comp_details;
    }
    
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getCurrentAccountNo()
    {
        $return = false;
        $qry = "SELECT `account_no` FROM `cio.company` ORDER BY id DESC LIMIT 1";

        $this->current_acc_no = $this->excuteQueryGET($qry)[0];

        if ($this->current_acc_no['account_no'] && (int) $this->current_acc_no['account_no'] > 0) {
            $return = (int) $this->current_acc_no['account_no'];
        }

        return $return;
    }

    public function getNewAccountNo()
    {
        $this->current_acc_no = $this->getCurrentAccountNo();

        if (!is_numeric($this->current_acc_no)) {
            return false;
        }

        $this->account_no = $this->current_acc_no + 1;
      
        return $this->account_no;
    }
   

    /**
     * updateCompanyById function
     * Update Company Params based on Account Id.
     *
     * @param array $comp_params
     * @return boolean
     */
    public function updateCompanyById(array $comp_params) :bool
    {
        $status = false;
        if (empty($comp_params) || !isset($comp_params['acc_id']) || (int) $comp_params['acc_id'] <= 0) {
            return $status;
        }

        $update = $this->getUpdateDetials();
        $qry = "UPDATE `cio.company` "
                ."SET `name`= '".$comp_params['company_name']."', "
                ." `street`='".$comp_params['address']."',"
                ." `house_number`='".$comp_params['house_no']."', "
                ." `postal_code`='".$comp_params['plz']."',"
                ." `city`='".$comp_params['city']."', "
                ." `country`='".$comp_params['country']."',"
                ." `website`='".$comp_params['website']."', "
                ." `url_option` ='".$comp_params['url_option']."',"
                ." `delegated_domain` ='".$comp_params['delegated_domain']."',"
                ." `phone`='".$comp_params['phone']."',"
                ." `vat`='".$comp_params['vat']."',"
                ." `login_url`='".$comp_params['login_url']."',"
                ." `update`='".$update['date']."', "
                ." `updated_by`='".$update['updated_by']."', "
                ." `modification`='".$comp_params['company_name']."'"
                ."  WHERE `account_no`='".$comp_params['acc_id']."'";

        $result = $this->excuteQuerySET($qry);
        if (is_bool($result) && $result == true) {
            $status = $result;
        }

        return $status;
    }
    
    /**
     * getCompanyDetailsById function
     *
     * @param integer $company_id
     * @return array
     */
    public function getCompanyDetailsById(int $company_id) :array
    {
        $comp_details = [];
        if (!isset($company_id) || $company_id <= 0) {
            return $comp_details;
        }

        $qry = "SELECT * FROM `cio.company` WHERE `id` = ".$company_id;
        $result = $this->excuteQueryGET($qry)[0];
        if (is_array($result) && !empty($result)) {
            $comp_details = $result;
        }

        return $comp_details;
    }
    
    
    /**
     * getAllUsersOfCompany function
     *
     * @param integer $company_id
     * @return array
     */
    public function getAllUsersOfCompany(int $company_id):array
    {
        $usr_list = [];

        if (!isset($company_id) || $company_id <= 0) {
            return $usr_list;
        }

        $qry ="SELECT * FROM `cio.users` WHERE `company_id` = ".$company_id;
        $result = $this->excuteQueryGET($qry);

        if (is_array($result) && !empty($result)) {
            $usr_list = $result;
        }

        return $usr_list;
    }
   
    /**
     * validateFreeDomains function
     * Validates the Company Domain with free domains in listed in Array.
     *
     * @param string $domain
     * @return boolean
     */
    public function validateFreeDomains(string $domain):  bool
    {
        $status = false;
        $list_domains = [
            /* Default domains included */
            "aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com",
            "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com",
            "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk",
  
            /* Other global domains */
            "email.com", "fastmail.fm", "games.com" /* AOL */, "gmx.net", "hush.com", "hushmail.com", "icloud.com",
            "iname.com", "inbox.com", "lavabit.com", "love.com" /* AOL */, "outlook.com", "pobox.com", "protonmail.com",
             "safe-mail.net", "wow.com" /* AOL */, "ygm.com" /* AOL */,
            "ymail.com" /* Yahoo */, "zoho.com", "yandex.com",
  
            /* United States ISP domains */
            "bellsouth.net", "charter.net", "cox.net", "earthlink.net", "juno.com",
  
            /* British ISP domains */
            "btinternet.com", "virginmedia.com", "blueyonder.co.uk", "freeserve.co.uk", "live.co.uk",
            "ntlworld.com", "o2.co.uk", "orange.net", "sky.com", "talktalk.co.uk", "tiscali.co.uk",
            "virgin.net", "wanadoo.co.uk", "bt.com",
  
            /* Domains used in Asia */
            "sina.com", "sina.cn", "qq.com", "naver.com", "hanmail.net", "daum.net", "nate.com", "yahoo.co.jp", "yahoo.co.kr", "yahoo.co.id", "yahoo.co.in", "yahoo.com.sg", "yahoo.com.ph", "163.com", "126.com", "aliyun.com", "foxmail.com",
  
            /* French ISP domains */
            "hotmail.fr", "live.fr", "laposte.net", "yahoo.fr", "wanadoo.fr", "orange.fr", "gmx.fr", "sfr.fr", "neuf.fr", "free.fr",
  
            /* German ISP domains */
            "gmx.de", "hotmail.de", "live.de", "online.de", "t-online.de" /* T-Mobile */, "web.de", "yahoo.de",
  
            /* Italian ISP domains */
            "libero.it", "virgilio.it", "hotmail.it", "aol.it", "tiscali.it", "alice.it", "live.it", "yahoo.it", "email.it", "tin.it", "poste.it", "teletu.it",
  
            /* Russian ISP domains */
            "mail.ru", "rambler.ru", "yandex.ru", "ya.ru", "list.ru",
  
            /* Belgian ISP domains */
            "hotmail.be", "live.be", "skynet.be", "voo.be", "tvcablenet.be", "telenet.be",
  
            /* Argentinian ISP domains */
            "hotmail.com.ar", "live.com.ar", "yahoo.com.ar", "fibertel.com.ar", "speedy.com.ar", "arnet.com.ar",
  
            /* Domains used in Mexico */
            "yahoo.com.mx", "live.com.mx", "hotmail.es", "hotmail.com.mx", "prodigy.net.mx",
  
            /* Domains used in Brazil */
            "yahoo.com.br", "hotmail.com.br", "outlook.com.br", "uol.com.br", "bol.com.br", "terra.com.br", "ig.com.br", "itelefonica.com.br", "r7.com", "zipmail.com.br", "globo.com", "globomail.com", "oi.com.br"
        ];

        if (empty($domain)) {
            return $status;
        }

        if (in_array($domain, $list_domains)) {
            return $status;
        }
        $status = true;

        return $status;
    }

    /**
     * getCompanyAccountDetials
     *
     * @param $id    : Company Id in `cio.company`
     *        'NULL' : Return the account detials for all the companies
     *
     * @return Array ;
     *         Array : Array of account details for all the companies or single company
     */
    public function getCompanyAccountDetials($id = null)
    {
        $qry = empty($id)? "SELECT `id`,`name`,`account_no` FROM `cio.company`" : "SELECT `id`,`name`,`account_no` FROM `cio.company` WHERE `id` = ".$id;
        if (empty($qry)) {
            return false;
        }
        $result = $this->excuteQueryGET($qry);
        return $result;
    }
}
    
    

    
    // public function getAllCompanyDetials(){
        
    //     $qry = "SELECT * FROM `cio.company`";
    //     $result = $this->excuteQueryGET($qry);
    //     var_dump($result);
    //     return $result;
    // }

    // /**
    //  * getCompanyDetailsByAccountId function
    //  *
    //  * @param integer $acc_id
    //  * @return array
    //  */
    // public function getCompanyDetailsByAccountId(int $acc_id) : array{

    //     $acc_details = [];
    //     if($acc_id <= 0){
    //         return $acc_details;
    //     }

    //     $sql = "SELECT * FROM `cio.company` WHERE `account_no` = ".$acc_id;
    //     $result = $this->excuteQueryGET($sql)[0];
    //     if(is_array($result) && (int)$result['id'] > 0){
    //         $acc_details = $result;
    //     }

    //     return $acc_details;
    // }
    
    // public function deleteCompany(){
        
    // }
