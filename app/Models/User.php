<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Pradeep
 */

namespace App\Models;

class User extends Model
{
    public $email;
    public $user_id;
    public $company_id;
    public $query;
   
    
    /**
     * createUser function
     * - Creates User after creating Company
     * - returns User details after creating User.
     *
     * @param array $params
     * @return array
     */
    public function createUser(array $usr_params) : array
    {
        $usr_details = 0;
        // Validates Invalid Params
        if (empty($usr_params) || empty($usr_params['email'])) {
            return $usr_details;
        }
        // Check if the User present in DB
        $detials = $this->getUserDetailsByEmail($usr_params['email']);
        if (is_array($detials) && (int)$detials['id'] > 0) {
            $usr_details = $detials;
            return $usr_details;
        }

        $usr_params['status'] = "new";              // Creates new User in DB.
        $update = $this->getUpdateDetials();        // Created and Updated are same while creating a row in DB
        //Encrypting user passwords
        $password = $usr_params['password'];
        $pepper = '2015';
        $salt = 'treactionA.G';
        $peperPassword = ($pepper . $password . $salt ); //This is our key
        $hashed_password = password_hash($peperPassword , PASSWORD_DEFAULT);
        
        $data =  [
            'company_id' => $usr_params['company_id'],
            'email' => $usr_params['email'],
            'password' => $hashed_password,
            'first_name' => $usr_params['first_name'],
            'last_name' => $usr_params['last_name'],
            'salutation' => $usr_params['salutation'],
            'status' => $usr_params['status'],
            'created' => $update['date'],
            'update' => $update['date'],
            /*
            'registration' => $params['registration'],
            'login_date' => $params['login_date'],
            'phone' => $params['phone'],
            'mobile' => $params['mobile'],
            'failed_logins' => $params['failed_logins'],
            'stauts' => $params['stauts'],
            'created' => $params['created'],
            'created_by' => $params['created_by'],
            'update' => $params['update'],
            'updated_by' =>  $params['updated_by']*/
        ];

        $sql = "INSERT INTO `cio.users`(`company_id`,`email`, `password`, `first_name`, `last_name`, `salutation`, `status`,`update`,`created`) "
            . "VALUES (:company_id, :email, :password, :first_name, :last_name, :salutation, :status, :update, :created )";

        $result = $this->excuteQuerySET($sql, $data);

        
        // Get the last inserted from table
        if (is_bool($result) && $result == true) {
            $usr_id = $this->getLastestRecordFromTable('cio.users', 'id');
        }

        return $usr_id;
    }
    
    public function activateUser($params)
    {
        $status = false;
        $qry = "UPDATE `cio.users` set `status` = 'active' where `id` = ";

        if (empty($params) || empty($params['user_id'])) {
            return false;
        }
        
        $qry .= $params['user_id'];
        $result = $this->excuteQuerySET($qry);

        if (!is_bool($result)) {
            return false;
        }
        return $result;
    }

    public function blockUser($params)
    {
        $qry = "UPDATE `cio.users` set `status` = 'blocked' where `id` = ";
        
        try {
            if (empty($params) || empty($params['id'])) {
                throw new \InvalidArgumentException('Empty arguments passed to Block User');
            }
            
            $qry .= $params['id'];
            $result = $this->excuteQuerySET($qry);
        } catch (Exception $e) {
            echo "Caught Exception:". $e->getMessage();
            die();
        }
       
        return $result;
    }
    
    public function resetFailedLogins($params)
    {
        $qry = "UPDATE `cio.users` SET `failed_logins` = '0' WHERE `id` = ";
        
        if (empty($params['id'])) {
            return false;
        }
        
        $qry .= $params['id'];
        
        $result = $this->excuteQuerySET($qry);
        
        if (!is_bool($result)) {
            return false;
        }
        
        return $result;
    }
    
    public function unBlockUser($params)
    {
        $qry = "UPDATE `cio.users` SET `status` = 'active' WHERE `id` = ";

        try {
            if (empty($params) || empty($params['id'])) {
                throw new \InvalidArgumentException('Empty arguments passed to UnBlock User');
            }
            $qry .= $params['id'];

            $result = $this->excuteQuerySET($qry);
           
            if (!is_bool($result)) {
                return false;
            }
           
            if (!$result || !$this->resetFailedLogins($params)) {
                return false;
            }
        } catch (Exception $ex) {
            echo "Caught Exception:". $e->getMessage();
            die();
        }
       
        return $result;
    }
    
    public function getUserStatus($params)
    {
        $qry ="SELECT status FROM `cio.users` WHERE id = ";
        
        if (empty($params['id'])) {
            return false;
        }
        $qry .= $params['id'];
        $result = $this->excuteQueryGET($qry);
        
        if (empty($result[0]["status"])) {
            return false;
        }
        return $result[0]["status"];
    }
        
    
//    public function updatePassword($params){
//
//        try{
//
//            if(empty($params) || empty($params['password'])){
//                throw new \InvalidArgumentException("Empty arguments passed to updatePassword");
//            }
//            $password = password_hash($params['password'], PASSWORD_DEFAULT);
//            $user_id = $params['id'];
//            $acc_id = $params['company_id'];
//            $update = $this->getUpdateDetials($params);
//            $qry = "UPDATE `cio.users` set `password` = '$password', `update`='".$update['date']."',`updated_by`='".$params['id']."'  where `id` = $user_id AND `company_id` = $acc_id";
//            $result = $this->excuteQuerySET($qry);
//
//            if(!$result){   #return if the query failed
//
//                return false;
//            }
//            if(!($this->getUserStatus($params) == "blocked" && $this->unBlockUser($params)) ){  # return if the query failed
//
//                return false;
//            }
//
//        } catch (Exception $e) {
//
//            echo "Caught Exception : " . $e->getMessage();
//        }
//
//        return true;
//    }
    
    /**
     * updatePassword function
     *
     * @param array $params
     * @return boolean
     */
    public function updatePassword(array $params): bool
    {
        $return = false;

        if (!is_array($params) || !is_int($params['user_id']) || $params['user_id'] <= 0) {
            die("updatePassword_1");
            return $return;
        }

        

        $cp_hash = password_hash($params['c_password'], PASSWORD_DEFAULT);
        $np_hash = password_hash($params['n1_password'], PASSWORD_DEFAULT);
        $u_data = $this->getUserDetailsById($params['user_id']);

        // if(!is_array($u_data) || empty($u_data['password']) || $cp_hash !== $u_data['password']){
        //     #die("updatePassword_2");
        //     return $return;
        // }

        $sql = "UPDATE `cio.users` SET `password` = '$np_hash' WHERE `id` = '".$params['user_id']."'";
        $result = $this->excuteQuerySET($sql);
        if (is_bool($result) && $result == true) {
            $return = true;
        }

        return $return;
    }

    
    /**
     * updateUserById function
     *
     * @param array $usr_params
     * @return boolean
     */
    public function updateUserById(array $usr_params) : bool
    {
        $status = false;
        if (empty($usr_params) || (int)$usr_params['user_id'] <= 0 || !is_numeric($usr_params['user_id'])) {
            return status;
        }
        
        $update = $this->getUpdateDetials();

        $qry = "UPDATE `cio.users` "
                ." SET `first_name`='".$usr_params['first_name']."',`last_name`='".$usr_params['last_name']."', "
                ." `salutation`='".$usr_params['salutation']."',`phone`='".$usr_params['phone']."', "
                ." `mobile`='".$usr_params['mobile']."',`update`='".$update['updated']."',`updated_by`='".$update['updated_by']. ""
                ."' WHERE id='".$usr_params['user_id']."'";
        
        $result = $this->excuteQuerySET($qry);
        if (is_bool($result) && $result == true) {
            $status = $result;
        }
        
        return $status;
    }
   
    /**
     * getUserDetailsById function
     *
     * @param integer $user_id
     * @return array
     */
    public function getUserDetailsById(int $user_id):array
    {
        $usr_details = [];

        if ($user_id <= 0) {
            return $usr_details;
        }

        $qry = "SELECT * FROM `cio.users` WHERE `id` = ".$user_id;
        $result = $this->excuteQueryGET($qry)[0];

        if (is_array($result) && !empty($result)) {
            $usr_details = $result;
        }

        return $usr_details;
    }
}
