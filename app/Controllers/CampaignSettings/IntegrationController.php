<?php

namespace App\Controllers\CampaignSettings;

use Slim\Http\UploadedFile;
use Slim\Http\Response;
use Slim\Http\Request;

/**
 * Class IntegrationController
 * @package App\Controllers\CampaignSettings
 *
 * This Controller is for mapping , email , digistore ,webinaris;
 */
class IntegrationController extends CampaignSettingsController
{

    /**
     * getMappingById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getMappingById(Request $request, Response $response): Response
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $name = 'Mapping';
        $group_name = 'Integrations';
        $values = [];
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_mp = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/maping.twig',
            ['c_set' => $info_mp]
        );
    }

    /**
     * postMappingById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function postMappingById(Request $request, Response $response): Response
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = 'POST';
        $name = 'Mapping';
        $group_name = 'Integrations';
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_mp = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/maping.twig',
            array('c_set' => $info_mp)
        );
        //return $response->write("success");
    }

    /**
     * getEmailDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getEmailDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $name = 'Mapping';
        $group_name = 'Integrations';
        $values = [];
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_mp = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/email.twig',
            ['c_set' => $info_mp]
        );
    }

    /**
     * postEmailDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function postEmailDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = 'POST';
        $name = 'Email';
        $group_name = 'Integrations';
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/email.twig',
            ['c_set' => $info_hb]
        );
    }

    /**
     * getWebinarisDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getWebinarisDetailsById(Request $request, Response $response) :Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $name = 'Webinaris';
        $group_name = 'Integrations';
        $values = [];
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/webinaris.twig',
            ['c_set' => $info_hb]
        );
    }

    /**
     * postWebinarisDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function postWebinarisDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = 'POST';
        $name = 'Webinaris';
        $group_name = 'Integrations';
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/webinaris.twig',
            ['c_set' => $info_hb]
        );
    }

    /**
     * getDigistoreDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getDigistoreDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $name = "Digistore";
        $group_name = "Integrations";
        $values;
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/digistore.twig',
            array('c_set' => $info_hb)
        );
    }

    /**
     * postDigistoreDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function postDigistoreDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests =[];
        $httpMethod = "POST";
        $name = "Digistore";
        $group_name = "Integrations";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/digistore.twig',
            array('c_set' => $info_hb)
        );
    }

    /**
     * getMailInOneDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getMailInOneDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $name = "MailInOne";
        $group_name = "Integrations";
        $values=[];
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/mailinone.twig',
            array('c_set' => $info_hb)
        );
    }

    /**
     * postMailInOneDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function postMailInOneDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $name = "MailInOne";
        $group_name = "Integrations";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/mailinone.twig',
            array('c_set' => $info_hb)
        );
    }

    /**
     * gethubspotDetailsById function
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getHubspotDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests =[];
        $httpMethod = 'GET';
        $name = 'Hubspot';
        $group_name = "Integrations";
        $values = [];
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/hubspot.twig',
            array('c_set' => $info_hb)
        );
    }

    /**
     * postHubspotDetailsById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postHubspotDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = 'POST';
        $name = 'Hubspot';
        $group_name = 'Integrations';
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/hubspot.twig',
            array('c_set' => $info_hb)
        );
    }

    /**
     * gethubspotDetailsById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getHTTPGetDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $name = 'HTTPGet';
        $group_name = 'Integrations';
        $values =[];
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_hb = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/HttpGet.twig',
            ['c_set' => $info_hb]
        );
    }

    /**
     * postHubspotDetailsById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postHTTPGetDetailsById(Request $request, Response $response) : Response
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests= [];
        $httpMethod = "POST";
        $name = "HTTPGet";
        $group_name = 'Integrations';
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $name,$group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/integrations/HttpGet.twig',
            ['c_set' => $info_hb]
        );
    }
}
