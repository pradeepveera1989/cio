<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\CampaignSettings;

use App\Controllers\Controller;
use Slim\Http\UploadedFile;

/**
 * Description of GeneralSettingsController
 *
 * @author Pradeep
 */
class CampaignSettingsController extends Controller
{

    /**
     * getInfo
     *
     * @param string $httpMethod
     * @param array $requests
     * @param string $name
     * @param string $group_name
     * @param array|null $values
     * @return array $info_settings = variable need to send to twig files
     */
    public function getInfo(string $httpMethod, array $requests, string $name, string $group_name, array $values = null)
    : array
    {
        
        $track_settings = [];
        if (empty($httpMethod) || empty($name) || empty($group_name) || empty($requests)) {
            return $track_settings;
        }

        if ($httpMethod === 'POST') {
            //object of the Model file
            $in_Obj = new \App\Models\CampaignParams($this->container->dbh, $name);
            //making params to send to model
            $params = $this->createParams($requests, $name, $group_name, $values);
            //sending $params to model
            $result = $in_Obj->insertOrUpdateSettings($params);
            //receving
            $track_settings = $this->getValuesFromDB($requests, $name);

            if (!\is_array($track_settings) || empty($track_settings) || !\is_bool($result) || $result === false) {
                $track_settings['error'] = 'Something went wrong, Failed to fetch Information';
            }
            $track_settings['success'] = 'General Settings details have been saved successfully';
        } elseif ($httpMethod === 'GET') {
            //getting information from models
            $track_settings = $this->getValuesFromDB($requests, $name);
        }
        return $track_settings;
    }


    /**
     * getValuesFromDB function
     *
     * @param array $requests
     * @param string $name
     * @return array
     */
    public function getValuesFromDB(array $requests, string $name) : array
    {
        $track_values = [];

        if (empty($name) || (int)$requests['camp_id'] <= 0 || (int)$requests['acc_id'] <= 0) {
            return $track_values;
        }

        $camp_id = $requests['camp_id'];
        $acc_id = $requests['acc_id'];
        $camp_Obj = new \App\Models\CampaignParams($this->container->dbh, $name);
        $s_details = $camp_Obj->getSettingDetails($camp_id)[0];

        $id = $s_details['id'];
        //string to object
        $track_values = json_decode($s_details[ 'value' ], true);
        //typecasting to array
        $track_values = (array)$track_values;
        $track_values['camp_id'] = $camp_id;
        $track_values['acc_id'] = $acc_id;
        $track_values['id'] = $id;

        return $track_values;
    }

    /**
     * createParams function
     *
     * @param array $requests
     * @param string $name
     * @param array $values
     * @return array
     */
    public function createParams(array $requests, string $name, string $group_name, array $values)
    : array
    {
        $params = [];
        $date = date('Y-m-d h:i:s', time());
        if (empty($values) || empty($name) || empty($group_name)||
            (int)$requests['camp_id'] <= 0 || (int)$requests['acc_id'] <= 0) {
            return $params;
        }

        $value = json_encode($values, JSON_UNESCAPED_UNICODE);
        //gettings campaign and account details
        $camp_id = $requests['camp_id'];
        $params['updated'] = $date;
        //Making params to send to databse
        //only this change according to requirments
        $params['id'] = $values['id'];
        $params['camp_id'] = $requests['camp_id'];
        $params['acc_id'] = $requests['acc_id'];
        $params['group_name'] = "Tracking" ;
        $params['name'] = $name ;
        $params['type'] = $group_name;
        $params['value'] = $value;

        return $params;
    }
}
