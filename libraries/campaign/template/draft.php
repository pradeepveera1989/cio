<?php
// Get the config data from config.ini       

require 'config.php';

// Get the config data from config.ini       
$conf_obj = new config('config.ini');
$config = $conf_obj->getAllConfig();

$currentUrl = parse_url($_SERVER['REQUEST_URI']);

// Keep URL Parameters
$redirect_path = $config[GeneralSetting][KeepURLParameter] ? $config[GeneralSetting][Redirect][RedirectPath] . $currentUrl["query"] : $config[GeneralSetting][Redirect][RedirectPath];
// Redirect Time
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = $_GET['trafficsource'] === "webgains" ? "true" : "false";
$lead_reference = empty($_GET['lead_reference']) ? " " : $_GET['lead_reference'];

?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">  
	<?php if($config[GeneralSetting][Redirect][RedirectStatus]){ ?>  
    <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
	<?php } ?>
    <title>EMS Training zu Hause</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
       <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/style.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
            
        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>          
        
    </head>


    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once'clients/tracking/outbrain/outbrain_redirect.php' : ' '; ?>

    <body>
        <!--Adeblo Tracking-->
        <?php $config[TrackingTools][EnableAdeblo] ? include_once'clients/tracking/adeblo/adeblo.php' : ' ' ?>
        
        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_redirect.php' : ' '; ?> 
 
    <!-- FORM -->
    <section class=" form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
    <div class="container">  
    <div class="row">
    <div class=" tre-content col-sm-12">
        <center>
            
                     
               <p>&nbsp;</p>
                <h1 style="font-size: 40px; font-weight: 600; font-family: 'Titillium Web', sans-serif; color:#fff;"><p>Bitte entschuldigen Sie die Unannehmlichkeiten. <br />Diese Website befindet sich aktuell im Wartungsmodus.<br />Wir bitten um Ihr Verständnis.</p></h1>
                 <p>&nbsp;</p>
                <p style="font-size: 26px; font-weight: 600; font-family: 'Titillium Web', sans-serif; color:#fff;">
                </p>
            <p>&nbsp;</p>
            
        </center>
    </div>
    </div>
    </div>  

</section>  
  

            <!-- </Webgains Tracking Code> -->    
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_redirect.php' : ' '; ?>       
      
    <!-- </Webgains Tracking Code NG> -->
    <!-- </Webgains Tracking Code> --> 

      
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>
