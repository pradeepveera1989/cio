/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {


    // Set the date we're counting down to
    var countDownDate = new Date(countdown_end_date).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("demo").innerHTML = days + " DAYS " + hours + " HOURS " +
            minutes + " MINUTES " + seconds + " SECONDS ";

        // If the count down is finished, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = countdown_expire_message;
        }
    }, 1000);


    $('.close').click(function() {
        $('#modal-video').hide();
        $('#modal-video iframe').attr("src", jQuery("#modal-video iframe").attr("src"));
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 660) {
            $(".nav-bottom").addClass("change"); // you don't need to add a "." in before your class name
        } else {
            $(".nav-bottom").removeClass("change");
        }
    });
   
    
    // telefon validation 
    var settings = {
        utilsScript: "vendor/telefonvalidator-client/build/js/utils.js",
        preferredCountries: ['de'],
        onlyCountries: ['de', 'ch', 'at'],
    };

    $('#telefon').intlTelInput(settings);
    $('#telefon-mobile').intlTelInput(settings);
    
    

    // Error messages
    $('.errorplz').hide();
    $('.errortelefon').hide();
    
    

    //Cookie 
    if ($.cookie('cookie[email]')) {
//        disableContactForm(single_submit_text);
    }
    
    

    // Countdown counter
    if (countdown_status != 1 || current_stocks <= 0) {
        //disableContactForm(countdown_expire_message);
    }
    
    

    function disableContactForm(err_msg) {
        $("input.input-fields").prop('disabled', true);
        $("input[type=radio]").prop('disabled', true);
        $("input[type=checkbox]").prop('disabled', true);
        $("span.countdown-nav").css('display', 'none');
        $("span.countdown-nav:last-child").replaceWith("<span style='color:#fff;font-size:16;text-transform:uppercase'>" + err_msg + "</span>");
    }



    /* JQuery Blur event for the postal code
     * Parameters : 
     * Returns : 
     *    1. fill the city Name on success.
     */
    $('.plz').blur(function() {
        var zip = $(this).val();
        var city;
        console.log(zip);
        if ($('.plz').hasClass('wrongplz')) {
            $('.plz').removeClass('wrongplz');
            $('.errorplz').hide();
        }
        // Check for autofill postal code value
        if (autofill_postal_code) {
            city = checkZipCode(zip);
            if (city) {
                $('.ort').val(city);
            } else {
                $('.ort').val("");
                $('.plz').addClass("wrongplz");
            }
        }
    });



    /* Function Name : checkZipCode
     * Parameters : zipcode
     * Returns : 
     *    1. countryName on success .
     *    2. False on faiure.
     */

    function checkZipCode(zip) {
        var status = true;
        var city;
        if (zip.length == 5 && localization_postal_code) {
            var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address='+ localization_postal_code +'+'+ zip +'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
            $.getJSON({
                url: gurl,
                async: false,
                success: function(response, textStatus) {
                    // check the status of the request
                    if (response.status !== "OK") {
                        status = false; // Postal code not found or wrong postal code.
                    } else {
                        // Postal code is found
                        status = true
                        var address_components = response.results[0].address_components;
                        $.each(address_components, function(index, component) {
                            var types = component.types;
                            // Find the city for the postal code
                            $.each(types, function(index, type) {
                                if (type == 'locality') {
                                    city = component.long_name;
                                    status = true;
                                }
                            });
                        });
                    }
                }
            });
        } else {
            status = false;
        }
        if (status) {
            return city;
        } else {
            return false;
        }
    }
    
    /* Function Name : Telefon
     * Parameters : Telefon number on blur field from HTML field .
     * Returns : 
     *    1. Validates the telefon number.
     *    2. False on faiure.
     */    

    $('#telefon-mobile').blur(function() {
        /* Regex for all the special characters and alphabits */
        alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
        tel = $('#telefon-mobile');
        console.log(tel);
        tel2 = $('#tel2');
        telnum = tel.val();
        var error = false;
        // If telefon validation true 
        if ($.trim(tel.val()) && validate_telefon) {
            // Check for alphabits and special characters from regex expresssion
            for (var i = 0; i < telnum.length; i++) {
                var c = telnum.charAt(i);
                if (alpha.test(c)) {
                    error = true;
                }
            }
            if (telnum.charAt(0) == 0) {
                console.log("Inside");
                telnum = telnum.substring(1);
            }
            console.log("After removiing", telnum)
            // Get the Conuntry code     
            var ext = tel.intlTelInput("getSelectedCountryData").dialCode;
            console.log("extention", ext);
            var num = "+" + ext + ' ' + telnum;
            console.log("num", num);
            tel2.val(num);
            // Check for valid number  
            if (!tel.intlTelInput("isValidNumber")) {
                $('.errortelefon').show();
            } else {
                if (error) {
                    $('.errortelefon').show();
                } else {
                    $('.errortelefon').hide();
                }
            }
        }else {
            $('.errortelefon').show();
        }
    });
    
    

    /* JQuery submit event for the postal code
     * Parameters : 
     * Returns : 
     *    1. check the postal code and accordingly display the error message for postal code.
     */
    form = $('.check-submit-form');
    form.submit(function(e) {
        //e.preventDefault();
        var form = $(this);
        var readyforsubmit = true;
        var plz = $('.plz');
        var ort = $('.ort');
        var email = $('.email');
        if (autofill_postal_code) {
            if (plz.hasClass("wrongplz")) {
                $('.errorplz').show();
                return false;
            }
        }
        if (validate_telefon) {
            if ($('.errortelefon').css('display') !== 'none') {
                return false;
            }
        }
        form.find("input[type='submit']").prop('disabled', true);
    });
});
