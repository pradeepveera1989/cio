<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of autoload
 *
 * @author Pradeep
 */

//

class Autoload
{
    public static $path = "../clients";
    
    public static function hubspotLoader($class)
    {
        require 'hubspot/'.$class.'.php';
    }

    public static function maileonLoader($class)
    {
        require 'maileon/'.$class.'.php';        
    }

    public static function splitTestLoader($class)
    {
        require $class.'.php';               
    }    

    public static function generalFunctionLoader($class)
    {
        require $class.'.php';               
    }    
    
  public static function leadsLoader($class)
    {
        require 'leads/'.$class.'.php';               
    }    
    
    public static function quizLoader($class)
    {
        require $class.'.php';               
    }          
    
}

//spl_autoload_register('Autoload::hubspotLoader');
//spl_autoload_register('Autoload::maileonLoader');
//spl_autoload_register('Autoload::databaseLoader');
//spl_autoload_register('Autoload::DatabaseLoader');
