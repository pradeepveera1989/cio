<?php

/*
* maileonApiClient.php
*
* MailonApiClient used to create contact service or transactions.
*   1. Inserts or updates a contact data
*   2. Creates a contact Event.
*   
*/
#require '../../vendor/autoload.php';
class maileonApiClient{
    public $mailinone;
    public $doi;
    public $config;
    public $custom_params;
    public $stnd_params;
    public $hubspot_contact_id;
    public $email;

    
       
    
	function __construct() {     
        
        $config = new config("config.ini");
        $this->config_mailinone = $config->getConfigMailInOne();
        $this->config_mailinone_event = $config->getConfigMailInOneEvent();
        $this->getConfigParams();
	} 

 /**
  * @Function  getConfigParams.
  *
  * @Description create config parameter required for Maileon
  *
  * @return void
  * 
  */    
     
    private function getConfigParams(){
        
        try {

            if(empty($this->config_mailinone)){

                throw new Exception("Could not Load the Mailinone parameters");
            }       
            
            $this->mailinone = [
                'BASE_URI' => 'https://api.maileon.com/1.0',
                'API_KEY' => $this->config_mailinone["Mapikey"],
                'THROW_EXCEPTION' => true,
                'TIMEOUT' => 60,
                'DEBUG' => 'false' // NEVER enable on production                
            ];
            $this->doi = $this->config_mailinone["DOIKey"];
                        
        } catch (Exception $e) {
            
            $e->getMessage();
        }
    }

 /**
  * @Function  createContactService.
  *
  * @Description Initiate Contact Service to insert or update a contact in Maileon
  *
  * @return void
  * 
  */       
    private function createContactService() {

        try {
            if(count($this->mailinone) == 0){
                throw new Exception("Maileon: Invalid Config Parameters " . PHP_EOL);
            }
            
            $this->contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
            $this->contact_service->setDebug(false);
            
            com_maileon_api_contacts_Permission::init();
            com_maileon_api_contacts_SynchronizationMode::init();
           
            $newContact = new com_maileon_api_contacts_Contact();
            $newContact->email = $this->email;
            $newContact->anonymous = false;
            $newContact->permission = $this->getPermission()["permission"];

            foreach ($this->stnd_params as $key => $value) {
                $newContact->standard_fields[$key] = $value;
            }
            
            foreach ($this->custom_params as $key => $value){
                if($this->checkCustomField($key)){                  
                    $newContact->custom_fields[$key] = $value;
                }
            }    
  
            foreach ($this->constant_params as $key => $value){
                if($this->checkCustomField($key)){
 
                    $newContact->custom_fields[$key] = $value;
                }    
            }    
            
            foreach ($this->url_params as $p){
                $l = explode("=", $p);     
                if($this->checkCustomField($l[0])){
                    $newContact->custom_fields[$l[0]] = $l[1];
                }    
            }             
            
#            $src = $this->campaign;
#            $subscription_page = urlencode($this->url);
            $this->response = $this->contact_service->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, $this->getPermission()["doi"], $this->doi);   

            if(!($this->response->isSuccess())){
                
               $this->response = false;
               throw new Exception("Maileon: Contact could not be created or updated ".PHP_EOL);     
            }
            
        } catch (Exception $e) {
             echo $e->getMessage(); 
        }       
    }
    
    public function intiateMarketingAutomation(){
      
        $program_id = $this->config_mailinone["MarketingAutomationId"];
        $email = $this->email;
        if(!is_numeric($program_id) && $program_id < 0){
            return false;
        }
        $markt_automate = new com_maileon_api_marketingautomation_MarketingAutomationService($this->mailinone);
        $status = $markt_automate->startMarketingAutomationProgram($program_id, $email);
        return $status;        
    }

    private function getPermission() {
        
        com_maileon_api_contacts_Permission::init();
        $permission = array(
            "doiplus"   =>  true,
            "permission" => com_maileon_api_contacts_Permission::$DOI_PLUS,
        );

        if($this->config_mailinone["DOI"]){
            $permission["doiplus"] = false;
            $permission["permission"] = com_maileon_api_contacts_Permission::$NONE;
        }
        return $permission;
    }
    
    public function DOIMarketingAutomation($email){
      
        $program_id = $this->config_mailinone["MarketingAutomationId"];
        if(!is_numeric($program_id) && $program_id < 0){
            return false;
        }
        $markt_automate = new com_maileon_api_marketingautomation_MarketingAutomationService($this->mailinone);
        $markt_automate->setDebug(false);
        $status = $markt_automate->startMarketingAutomationProgram($program_id, $email);
        return $status;        
    }

    /**
  * @Function  createTransaction.
  *
  * @Description Verifies the config and throw an exception if the config is missing.
  *              Initiates the transaction service with respective email adress.
  *
  * @return void
  * 
  */      
    
    private function createTransaction(){

        try {
            if(count($this->mailinone) == 0){
                throw new Exception("Maileon: Invalid Config Parameters ".PHP_EOL);     
            }
            
            $transactionsService = new com_maileon_api_transactions_TransactionsService($this->mailinone);   
            $transactionsService->setDebug(false);
            $transaction = new com_maileon_api_transactions_Transaction();
            $transaction->contact = new com_maileon_api_transactions_ContactReference($this->contact_service);
            $transaction->type = $this->type;
            $transaction->contact->email = $this->email;
            $transactions = array($transaction);
            foreach ($this->event_params as $key => $value){
                   $transaction->content[$key] = $value;
            }
            
            $response = $transactionsService->createTransactions($transactions, true, false);
        } catch (Exception $e) {
            echo $e->getMessage(); 
        }
    }

  /**
  * @Function  getContactStatus.
  *
  * @Description Verifies the Maileon where the contact is present or
  *              not based on Email address of the contact.
  *
  * @return contact Id on success
  */      
        
    private function getContactStatus($email){
        $return = false;
        
        $contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
        $contact_service->setDebug(false);
        $getContact = $contact_service->getContactByEmail($email);
        if(!($getContact->getResult()->id)){
            return $return;
        }
        $return = $getContact->getResult()->id;
        return $return;
    }

  /**
   * @Function  existsContact.
   * 
   * @param string $email
   * @return boolean
   */
    public function existsContact($email) {
        
         $result = FALSE;
        
        $contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
        $contact_service->setDebug(false);
        $getContact = $contact_service->getContactByEmail($email);
        if ($getContact->isSuccess()
                && ($getContact->getResult()->permission != '1'
                || $getContact->getResult()->permission != '2'
                || $getContact->getResult()->permission != '3'
                || $getContact->getResult()->permission != '6')
                ){
            $result = TRUE;
        }
       
        return $result;
    }

    
  /**
   * @Function  getEventParams.
   *            -  Maps the Standard Params to Config ini params
   * 
   * @param array $param
   * @return array 
   */    
    private function getEventParams($params){
      
        $event_params = $this->config_mailinone_event;
        $event_params = [];
        
        // Check for event type 
        if(empty($this->getEventType())){
            return [];
        }

        // Mapping the event params from ini file
        foreach ($this->config_mailinone_event[EventMapping] as $key => $value) {
            $event_params[$key] = $params[$value];
        }
        
        return $event_params;
    }
    

  /**
   * @Function  getEventType.
   * 
   * @param array $param
   * @return param event type 
   */      
    private function getEventType() {

        return $this->config_mailinone_event[ContactEvent];
    }  
  
  
  /**
  * @Function  createEventPartnerLead.
  *
  * @Description Intiate the contact event for the Partner Lead.
  *              1. Inserts the contact in the system if the contact is missing.
  *              2. Intiate the tranaction service with Partner contact Email adress 
  *            
  * @return array of parameter
  * 
  */        
    public function createEvent($params){

        $this->type = $this->getEventType($params); 
            
        try{
            
            // Contact needs to be updated all the time.
            $this->insertContact($params);
           
            if(!$this->response->isSuccess()){
                
                return false;
            }
            $this->type = $this->getEventType($params);
            $this->event_params = $this->getEventParams($params);
            
            if(!empty($this->event_params)){
            
                $this->createTransaction();
            }
            
        } catch (Exception $e) {
            echo $e->getMessage(); 
        } 
        
        return true;
    }    

  
  /**
  * @Function  getStandardParams.
  *
  * @Description Maps the Standard parameters required to create a contact in Maileon
  *            
  * @return array of parameter
  * 
  */        
    private function getStandardParams($param){
        
        $return = false;
        
        $stand_parms = $this->config_mailinone[Mapping];
        
        if(empty($param[$stand_parms[EMAIL]])){
            return $return;
        }
        $this->email = $param[$stand_parms[EMAIL]];
        
        return array(
            "SALUTATION" => $param[$stand_parms[SALUTATION]],
            "FIRSTNAME" => $param[$stand_parms[FIRSTNAME]],
            "LASTNAME" => $param[$stand_parms[LASTNAME]],
            "ADDRESS" => $param[$stand_parms[ADDRESS]],
            "ZIP" => $param[$stand_parms[ZIP]],
            "CITY" => $param[$stand_parms[CITY]]
        );
    }

    private function checkCustomField($field){
        
        if((!$this->contact_service) && !empty($field)){
            
            return false;
        }
        
        $getCustomFields = $this->contact_service->getCustomFields(); #Get all the Custom fields
        $this->custom_fields = array_keys($getCustomFields->getResult()->custom_fields);  
       
        # Return true if the contact field is present
        $serach_index = array_search($field, $this->custom_fields);

        if(is_numeric($serach_index) && $serach_index >= 0 ){   
            
            return true;
        } 
        
        # if the contact field is not present
        return false;
    }
    
    
    private function getCustomParams($params){

        
        $mapping_parms = $this->config_mailinone[Mapping];   
        $pv = array_values($params);                # Get the values of parms
        $pk = array_keys($params);                  # Get the keys of parms
        $return = array();
        $i = 0;
        while($i <= count($pk)){

//            var_dump("Mapping Params",$mapping_parms);
//            var_dump("value", $pv[$i]);
//            var_dump("Standard", $this->standard);  
//            var_dump("i", $i);
            
            $search_value = array_search($pk[$i], $mapping_parms);  # Search the key value in Mapping parms
//            var_dump("search value", $search_value);
            $not_standard = array_search($pv[$i], $this->standard); # Search the key value in Standard parms

            if(!empty(($search_value)) && empty($not_standard)){
                                
                $return[$search_value] = $pv[$i];                   # Custom keys shoud not be present in Standard parms
            }                                                       # but should be present in Mapping prams
       
            $i++;
        }

        return $return;
    }
    
    private function getConstantParams(){
        
        $constant_params = $this->config_mailinone[Constants];
               
        return $constant_params;
    }
    
    private function getURLParams($params){
        
        if(empty($params["url_params"])){
         
            return false;
        }
        return explode("&",$params["url_params"]);       
    }
    
    public function buildURL($params){
       $parameter = '?';
       $feld = array(
           'typ',
           'segment',
           'quelle',
           'trafficsource',
           'partner',
           'ip',
           'datum'
       );
       foreach ($params as $key => $param) {
           
                $parameter .= $key .'='.$param.'&';
           
       }
       return $parameter;
   }    

  /**
  * @Function  insertContact.
  *
  * @Description inserts the contact data to the by creating contact service.
  *              Maps the Standard and Custom parameters of the contact. 
  *              Intiates the contact service only if there are standard parameters.
  *            
  * @return response data of contact service.
  * 
  */     
    public function insertContact($params){
                      
        try{
            
            if(empty($params)){
                
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);     
            }
            
            $this->stnd_params = $this->getStandardParams($params);
            $this->custom_params = $this->getCustomParams($params);
            $this->constant_params = $this->getConstantParams();
            //$this->url_params = $this->getURLParams($params);
            $this->url = $params["url"].'v1.0.0/'.$this->buildURL($params);
            $this->campaign = $params["campaign_name"];
            if(!$this->stnd_params){  
                
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);     
            }
           
            $this->createContactService();
            $return = $this->response;
                        
        } catch (Exception $e) {
            echo $e->getMessage();    
        }
        return $return;        
    }
   
}

?>

