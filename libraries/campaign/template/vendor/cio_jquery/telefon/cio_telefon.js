/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    console.log("Validate TElefon", validate_telefon);
    // telefon validation 
//    if (validate_telefon) {
    var settings = {
        utilsScript: "vendor/telefonvalidator-client/build/js/utils.js",
        preferredCountries: ['de'],
        onlyCountries: ['de', 'ch', 'at'],
    };

    $("#telefon-mobile").intlTelInput(settings);

    /* Function Name : Telefon
     * Parameters : Telefon number on blur field from HTML field .
     * Returns : 
     *    1. Validates the telefon number.
     *    2. False on faiure.
     */

    $('#telefon-mobile').blur(function () {
        if (validate_telefon) {
            /* Regex for all the special characters and alphabits */
            alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
            tel = $('#telefon-mobile');
            tel2 = $('#tel2');
            telnum = tel.val();
            var error = false;
            // If telefon validation true 
            if ($.trim(tel.val()) && validate_telefon) {
                // Check for alphabits and special characters from regex expresssion
                for (var i = 0; i < telnum.length; i++) {
                    var c = telnum.charAt(i);
                    if (alpha.test(c)) {
                        error = true;
                    }
                }
                if (telnum.charAt(0) == 0) {
                    telnum = telnum.substring(1);
                }
                console.log("After removiing", telnum)
                // Get the Conuntry code     
                var ext = tel.intlTelInput("getSelectedCountryData").dialCode;

                var num = "+" + ext + ' ' + telnum;
                tel2.val(num);
                // Check for valid number  
                if (!tel.intlTelInput("isValidNumber")) {
                    $('.errortelefon').show();
                } else {
                    if (error) {
                        $('.errortelefon').show();
                    } else {
                        $('.errortelefon').hide();
                    }
                }
            } else {
                $('.errortelefon').show();
            }
        }
    });
//    }
});
