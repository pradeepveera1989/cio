<?php

/*
 *To send data to webinaris
 * @Author Aravind
 */
class webinaris
{
    public $webinaris;
    private $config;
    private $webinar;
    private $baseurl;
    private $webinarPassword;

    public function __construct()
    {
        $this->config = new config("config.ini");
        $this->webinar = $this->config->getConfigwebinaris();
        $this->webinarId= $this->webinar[webinarID];
        $this->webinarPassword = $this->webinar[APIPassword];
        $this->baseurl = "https://webinaris.co/api/";
    }
    /**
     * sendDataWebinaris:
     *
     * Sends data to webinarisAPI
     *
     * @param type $params
     * @return responce, true if sent data to webinarisAPI
     */
        
    public function sendDataWebinaris($params)
    {
        if ($params) {
            $config_mailinone = $this->config->getConfigMailInOne();
            $email = $params[$config_mailinone[Mapping][EMAIL]] ;
            $firstname = $params[$config_mailinone[Mapping][FIRSTNAME]] ;
            $lastname = $params[$config_mailinone[Mapping][LASTNAME]] ;
            $time = $params[$config_mailinone[Mapping][WebinarDate]];
            $ip = $params[$config_mailinone[Mapping][ip_adresse]];
            $paid =($this->webinar)? 'YES' : 'NO';

            if (isset($this->webinarId) && isset($this->webinarPassword)) {
                $url = $this->baseurl."?key=".$this->webinarId."&is_paid=".$paid."&time=".urlencode($time)."&email=".$email."&firstname=".$firstname."&lastname=".$lastname."&ip_address=".$ip."&password=".$this->webinarPassword;
                $responce =  $this->crulRequest($url);
                var_dump($response);
                die();
                return $responce;
            }
        }
    }
    /**
     * getDates:
     *
     * Sends data to webinarisAPI
     *
     * @return AvaliableDates
     */
    
    public function getDates()
    {
        $response = false;
        if (isset($this->webinarId) && isset($this->webinarPassword)) {
            $url = $this->baseurl."showtimes?webinaris_id=".$this->webinarId."&api_password=".$this->webinarPassword;
            $response =  $this->crulRequest($url);

            if ($response === false) {
                return $response;
            }
            $parsed_response = json_decode($response, true);
            if ($parsed_response['data']) {
                $response = $parsed_response['data'];
            }
        }
        return $response;
    }
    
    public function crulRequest($url)
    {
        $response = false;
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $ch = curl_init($url);
            $optArray = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array("Accept: application/json"),
            );
            curl_setopt_array($ch, $optArray);
            $response = curl_exec($ch);

            if ($response === false) {
                echo 'Curl error: ' . curl_error($ch);
            }
            curl_close($ch);
        }
        return $response;
    }
}
