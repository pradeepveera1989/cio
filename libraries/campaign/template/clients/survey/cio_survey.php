<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of survey
 *
 * @author Pradeep
 */

class cio_survey {

    public $campaign_id = "";
    public $table = "";

    function __construct($campaign_id) {
        $this->campaign_id = $campaign_id;
        $this->dbObject = new db();
        $this->custom_leads = new customLeads();
        $this->config = new config("config.ini");
    }

    public function getSurveyByCampaignId() {

        $return = [];
        $this->get_survey = [];
        $this->slider = [];
        $this->multiple_choice = [];
        $this->single_choice = [];
        $this->table = "`cio.questions`";
        var_dump("Campaign Id", $this->campaign_id);
        if ((int) $this->campaign_id <= 0 || empty(trim($this->campaign_id)) || empty(trim($this->table))) {
            return $return;
        }

        if (!$this->checkSurveyByCampaignId()) {
            return $return;
        }

        $sql = "SELECT `cio.questions`.`id`, `cio.questions`.`question`, `cio.questions`.`hint`, `cio.questions`.`datatype`, `cio.questions`.`question_type`,"
                . " `cio.answer_options`.`value`, `cio.answer_options`.`link_ref`, `cio.answer_options`.`correct`, `cio.answer_options`.`id` as answer_options_id\n"
                . "FROM `cio.questions`\n"
                . "INNER JOIN `cio.answer_options` ON `cio.questions`.`id` = `cio.answer_options`.`question_id`\n"
                . "WHERE`cio.questions`.`campaign_id` = " . $this->campaign_id;
        
        $result = $this->dbObject->getAll($sql);
        
        if(empty($result)){
            return $return;
        }
        
        foreach ($result as $svy) {

            if ($svy['question_type'] == "inputfield") {
                $this->getSurveryFormatQuestionTypeInputfield($svy);
            } elseif ($svy['question_type'] == "slider") {
                $this->getSurveryFormatQuestionTypeSlider($svy);
            } elseif ($svy['question_type'] == "multiple_choice") {
                $this->getSurveryFormatQuestionTypeMultiChoice($svy);
            } elseif ($svy['question_type'] == "single_choice") {
                $this->getSurveryFormatQuestionTypeSingleChoice($svy);
            }
        }      
        
        return $this->get_survey;
    }
    

    /*
     * getSurveryFormatQuestionTypeInputfield
     * 
     * @param
     *        $params : Survery for Inputfield
     * 
     * @return : Parsed Survey
     *      
     */

    public function getSurveryFormatQuestionTypeInputfield($svy_inputfield) {

        if (empty($svy_inputfield)) {
            return false;
        }
        
        if($svy_inputfield['question_type'] != "inputfield" || (int)($svy_inputfield['id']) <= 0 || empty(trim($svy_inputfield['question']))){
            return false;
        } 
        
        array_push($this->get_survey, $svy_inputfield);
    }    

    
    /*
     * getSurveryFormatQuestionTypeSlider
     * 
     * @param
     *        $params : Survery for Slider
     * 
     * @return : Parse the value, Max value and Min Value
     *      
     */

    public function getSurveryFormatQuestionTypeSlider($svy_slider) {

        if (empty($svy_slider) || $svy_slider['question_type'] != "slider") {
            return false;
        }

        if((int)($svy_slider['id']) <= 0 || !isset($svy_slider['question']) || (int)($svy_slider['answer_options_id']) <= 0 ){
            return false;
        }         
        
        $id_found = $this->searchQuestionIdInSurvey($svy_slider['id']);

        if (empty($this->slider) && !$id_found) {

            $this->slider["id"] = $svy_slider['id'];
            $this->slider['question'] = $svy_slider['question'];
            $this->slider['hint'] = $svy_slider['hint'];
            $this->slider['datatype'] = $svy_slider['datatype'];
            $this->slider['question_type'] = $svy_slider['question_type'];
            $this->slider['slider'][0] = ['id' => $svy_slider['answer_options_id'], 'value' => $svy_slider['value'], 'status' => $svy_slider['correct']];
        }

        if (!$id_found) {
            array_push($this->get_survey, $this->slider);
            unset($this->slider);
        } else {
            $search = array_search($svy_slider['id'], array_column($this->get_survey, 'id'));
            array_push($this->get_survey[$search]['slider'], ['id' => $svy_slider['answer_options_id'], 'value' => $svy_slider['value'], 'status' => $svy_slider['correct']]);
        }
       
        return True;
    }


    /*
     * getSurveryFormatQuestionTypeMultiChoice
     * 
     * @param
     *        $params : Survery for Multiple Choice
     * 
     * @return : Parse the options for Multiple choice question
     *      
     */

    public function getSurveryFormatQuestionTypeMultiChoice($svy_multiple) {

        if (empty($svy_multiple) || $svy_multiple['question_type'] != "multiple_choice") {
            return false;
        }
        if( (int)($svy_multiple['id']) <= 0 || !isset($svy_multiple['question']) || (int)($svy_multiple['answer_options_id']) <= 0 ){
            return false;
        }       
        $id_found = $this->searchQuestionIdInSurvey($svy_multiple['id']);

        if (empty($this->multiple_choice) && !$id_found) {

            $this->multiple_choice["id"] = $svy_multiple['id'];
            $this->multiple_choice['question'] = $svy_multiple['question'];
            $this->multiple_choice['hint'] = $svy_multiple['hint'];
            $this->multiple_choice['datatype'] = $svy_multiple['datatype'];
            $this->multiple_choice['question_type'] = $svy_multiple['question_type'];
            $this->multiple_choice['multiple_options'][0] = ['id' => $svy_multiple['answer_options_id'], 'link_ref' => $svy_multiple['link_ref'], 'value' => $svy_multiple['value'], 'status' => $svy_multiple['correct']];
        }

        if (!$id_found) {
            array_push($this->get_survey, $this->multiple_choice);
            unset($this->multiple_choice);
        } else {
            $search = array_search($svy_multiple['id'], array_column($this->get_survey, 'id'));
            array_push($this->get_survey[$search]['multiple_options'], ['id' => $svy_multiple['answer_options_id'], 'link_ref' => $svy_multiple['link_ref'], 'value' => $svy_multiple['value'], 'status' => $svy_multiple['correct']]);
        }

        return true;
    }

    /*
     * getSurveryFormatQuestionTypeSingleChoice
     * 
     * @param
     *        $params : Survery for Dropdown question
     * 
     * @return : Parse the options for Dropdown question
     *      
     */

    public function getSurveryFormatQuestionTypeSingleChoice($svy_single) {

        if (empty($svy_single) || $svy_single['question_type'] != "single_choice") {
            return false;
        }

        if((int)($svy_single['id']) <= 0 || !isset($svy_single['question']) || (int)($svy_single['answer_options_id']) <= 0 ){
            return false;
        } 
        
        $id_found = $this->searchQuestionIdInSurvey($svy_single['id']);

        if (empty($this->single_choice) && !$id_found) {

            $this->single_choice["id"] = $svy_single['id'];
            $this->single_choice['question'] = $svy_single['question'];
            $this->single_choice['hint'] = $svy_single['hint'];
            $this->single_choice['datatype'] = $svy_single['datatype'];
            $this->single_choice['question_type'] = $svy_single['question_type'];
            $this->single_choice['single_options'][0] = ['id' => $svy_single['answer_options_id'], 'value' => $svy_single['value'], 'status' => $svy_single['correct']];
        }

        if (!$id_found) {

            array_push($this->get_survey, $this->single_choice);
            unset($this->single_choice);
        } else {

            $search = array_search($svy_single['id'], array_column($this->get_survey, 'id'));
            array_push($this->get_survey[$search]['single_options'], ['id' => $svy_single['answer_options_id'], 'value' => $svy_single['value'], 'status' => $svy_single['correct']]);
        }
    }


    /*
     * searchQuestionIdInSurvey
     * 
     * @param
     *        $params : Search the question id with global survery ($this->survey)
     * 
     * @return : 
     *         'True' : If the question is found
     *         'False' : If question is not found
     */

    public function searchQuestionIdInSurvey($question_id) {

        if (empty($question_id) || empty($this->get_survey) || (int)$question_id <= 0 ) {

            return false;
        }

        foreach ($this->get_survey as $survey) {

            $id_found = in_array($question_id, $survey);

            if ($id_found) {
                // Return only if question id found
                return $id_found;
            }
        }
        
        return false;
    }

    
    /*
     * checkSurveyByCampaignId
     * 
     * @param
     *        $params : 
     * 
     * @return : 
     *           True   : If survey is Present
     *           False  : If Survey is not Present
     */    
    public function checkSurveyByCampaignId() {

        $result = false;
        $this->table = "`cio.questions`";

        if ((int) $this->campaign_id <= 0 || empty(trim($this->campaign_id)) || empty(trim($this->table))) {
            return $result;
        }

        $select = array('id');
        $where = array("campaign_id = '" . $this->campaign_id . "'");
        $table = $this->table;
        $result = $this->dbObject->getOne($select, $table, $where);

        if (empty($result) || empty(trim($result['id'])) || (int) $result['id'] <= 0) {
            $result = false;
        } else {
            $result = true;
        }

        return $result;
    }
    
    public function getRangeValuesForSlider($params){

        $slid_val = 0;
        $slid_val_1 = 0;
        $slid_val_2 = 0;
        $value = [
            'min_value' => 0,
            'max_value' => 0,
            'value' => 0
        ];
        if(empty($params) || $params['question_type'] != 'slider' || empty($params['slider'])){
            return $value;
        }

        foreach($params['slider'] as $slide_value){

            if(($slide_value['status'] == "WAHR") && empty(trim($slid_val))){
                $slid_val = $slide_value['value'];
            }
            if(empty(trim($slid_val_1))){
                $slid_val_1 = $slide_value['value'];
            }else if(!empty(trim($slid_val_1)) && empty(trim($slid_val_2))){
                $slid_val_2 = $slide_value['value'];
            }   
        }
        
        if(empty($slid_val_1) || empty($slid_val_2) || empty($slid_val)){
            return $value;
        }
        
        if($slid_val_1 < $slid_val_2){
            $value['min_value'] = $slid_val_1;
            $value['max_value'] = $slid_val_2;
        }else {
            $value['max_value'] = $slid_val_1;
            $value['min_value'] = $slid_val_2;
        }
        $value['value'] = $slid_val;

        return $value;
    }    
    
    
    /*
     * postSurveyByCampaignId
     * 
     * @param
     *        $params : 
     * 
     * @return : 
     *           True   : If survey is Present
     *           False  : If Survey is not Present
     */      
    public function postSurveyByCampaignId($params){

        $result = [];
        if(empty($params) || empty(trim($params['lead_reference']))){
                var_dump("postSurveyByCampaignId _ 1");
                die();            
            return $result;
        }
        
        // Example Lead number
        $custom_lead_id = '';
        $custom_lead_param['name'] = 'Survery_answer';
        $custom_lead_param['lead_reference'] = $params['lead_reference'];
   
        foreach($params as $param){

			if(empty(trim($param['question_type'])) || empty(trim($param['id']))){

				return $result;
			}	
            $custom_lead_param['type'] = $param['question_type'];
            $custom_lead_param['question_id'] = $param['id'];
            $custom_lead_param['question_type'] = $param['question_type'];

            switch ($param['question_type']){
                case 'inputfield':
                    if(empty($param['answer']['input_field'])){
                        continue;
                    }
                    $custom_lead_param['value'] = $param['answer']['input_field'];      
                    $this->insertAnswersToDatabase($custom_lead_param);
                    break;
         
                case 'multiple_choice':
                    if(empty($param['answer']['multiple_options']['options'])){
                        continue;
                    }
                    foreach($param['answer']['multiple_options']['options'] as $option){                     
                        $custom_lead_param['value'] = $option['value'];   
                        $this->insertAnswersToDatabase($custom_lead_param);
                    }    
                    break;
                    
                case 'single_choice':
                    if(empty($param['answer']['single_options'])){
                        continue;
                    }
                    $custom_lead_param['value'] = $param['answer']['single_options'];   
                    $this->insertAnswersToDatabase($custom_lead_param);
                    break;
                    
                case 'slider':
                    if(empty($param['answer']['slider'])){
                        continue;
                    }
                    $custom_lead_param['value'] = $param['answer']['slider'];   
                    $this->insertAnswersToDatabase($custom_lead_param);
                    break;
            }
        }

        return true;
    }
    
    
    /*
     * insertAnswersToDatabase
     * 
     * @param
     *        $params : Array
     * 
     * @return : 
     *           Id   : return the insert id of row
     *           False  : Insert is success
     */        
    private function insertAnswersToDatabase($params) {

        if(empty($params) || empty(trim($params['value'])) || empty(trim($params['question_type'])) || (int)($params['question_id']) <= 0 ) {
            var_dump("insertAnswersToDatabase");die();
            return false;
        }
        spl_autoload_register('Autoload::survey');
        $cio_answer = new cio_answers();   
        
        $custom_lead_id = $this->custom_leads->insertLeadCustomFields($params);
 
        if(!isset($custom_lead_id) || (int)$custom_lead_id <= 0){
            return false;
        }

        $answers = [
            'answer' => $params['value'],
            'lead_customfield_id' => $custom_lead_id,
            'question_id'=> $params['question_id'],
            'question_type'=> $params['question_type'],
        ];
        $cio_answer_id = $cio_answer->insertSurveyAnswers($answers);
        
        if(empty($cio_answer_id) || (int)$cio_answer_id <= 0){
            var_dump("cio_answer_id");die();
            return false;
        }
        
        return $cio_answer_id;
    }
}
