<?
    require "../../vendor/autoload.php";
    session_start();
    $answers = $_POST;
 

    if(is_array($answers) && !empty($answers)){
            
        // URL format has to be changed in Production 
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .'/'.  explode( '/', $_SERVER['REQUEST_URI'])[1];
        //$url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .'/'.  explode( '/', $_SERVER['REQUEST_URI'])[1] .'/'.  explode( '/', $_SERVER['REQUEST_URI'])[2] .'/'.  explode( '/', $_SERVER['REQUEST_URI'])[3];
        // Validate URL
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $camp_details = getCampaignDetailsForSurvey($url);
            $f_answers = formatSurveyAnswers($answers);

            // Post Survey Answers to Survey Object
            if ((int)$camp_details['cio.survey_id'] > 0 && (int)$camp_details['id'] > 0 && is_array($f_answers) && !empty($f_answers)){

                $conf_Obj = new config("config.ini");
                $config_survey = $conf_Obj->getConfigSurvey();
                $config = $conf_Obj->getAllConfig();
                if(is_array($config_survey) && !empty($config_survey) && $config_survey["Flow"] == "BeforeLeadRegistration"){
        
                    $_SESSION['survey'] = [
                        "survey_id"     => $camp_details['cio.survey_id'],
                        "campaign_id"   => $camp_details['id'],
                        "lead_ans"      => $f_answers
                    ];

                    $redirectURL = $url;

                }elseif (is_array($config_survey) && !empty($config_survey) && $config_survey["Flow"] == "AfterLeadRegistration"){

                    $survey = postSurveyJson($camp_details, $f_answers);

                    if(is_bool($survey) && $survey == TRUE){
                        $redirectURL = $config['GeneralSetting']['SuccessPage'];
                    }else{
                        $redirectURL = $config['GeneralSetting']['ErrorPage'];
                    }

                }else {
                    $redirectURL = $config['GeneralSetting']['ErrorPage'];
                }

                echo $redirectURL;
            }
        }    
    }


    /**
     * getSurveyIdFromURL function
     *
     * @param string $url
     * @return integer
     */
    function getCampaignDetailsForSurvey(string $url):array {

        $camp_details = [];

        if(empty($url)){
            die("getSurveyIdFromURL");
            return $camp_details;
        }

        spl_autoload_register('Autoload::campaignLoader');
        $c_obj = new campaign($url);
        $detials = $c_obj->getCampaignDetailsByURL();

        if(is_array($detials) && !empty($detials)){
            $camp_detials = $detials;
        }

        return $camp_detials;
    }

    /**
     * postSurveyJson function
     *
     * @param integer $s_id
     * @param array $answers
     * @return void
     */
    function postSurveyJson(array $camp_details ,array $answers){

        $status = false;
        if(empty($camp_details) || empty($answers) || (int)$camp_details['cio.survey_id'] <= 0 && (int)$camp_details['id'] <= 0){
            die("postSurveyJson");
            return $status;
        }   

        $s_id = (int)$camp_details['cio.survey_id'];
        $c_id = (int)$camp_details['id'];

        spl_autoload_register('Autoload::surveyLoader');
        $s_Obj = new postSurvey($s_id, $c_id);
        $status = $s_Obj->postSurveyAnswers($answers);   

        return $status;
    }

    /**
     * formatSurveyAnswers function
     *
     * @param array $answers
     * @return array
     */
    function formatSurveyAnswers(array $answers):array{

        $formt_ans = [];
        if(empty($answers)){
            return $formt_ans;
        }

        foreach($answers as $key => $value){
            $s['question_id'] = $key;
            $s['lead_answer'] = $value;
            array_push($formt_ans , $s);
        }

        return $formt_ans;
    }

?>
