<?php

/*
* hubspotApiClient.php
*
* HubspotApiClient does the following.
*   1. Creates a Contact in Hubspot
*   2. Creates a Deal in Hubspot
*   3. Creates a Company in Hubspot
*
*/

class hubspotApiClient
{
    public $contact_id;
    public $company_id;
    public $deal_id;
    public $key;
    public $status;
    public $properties = array();
    
    public function __construct()
    {
        $config = new config("config.ini");
        $this->config_hubspot = $config->getConfigHubspot();
        $this->config_mailinone = $config->getConfigMailInOne();
        $this->key = $this->config_hubspot[hubapi];

        #$this->getAllProperties();
    }

    /**
    * @Function  validateHubspotKey.
    *
    * @Description Validates the Hubspot key by trying to create an object using the Key.
    *
    * @return void
    *
    */
    private function validateHubspotKey($key)
    {
        try {
            $hb = new HubSpot_Deal($key);
            $res = $hb->get_recent_deals()->results;
            if (is_null($res)) {
                throw new Exception("Hubspot : Invalid Hubspot Key".PHP_EOL);
            }
        } catch (Exception $e) {
            $this->errorKey = true;
            echo $e->getMessage();
        }
    }


    /**
    * @Function  getContactParams.
    *
    * @Description Maps the contact Parameters required to create a contact.
    *
    * @return array of contact parameters.
    *
    */
    private function getContactParams($params)
    {
        $hb_params = $this->config_mailinone[Mapping];
        $hb_contact = $this->config_hubspot[Contact];
        $hbc_val = array_values($hb_contact);
        $hbc_key = array_keys($hb_contact);
        $hbp_val = array_values($hb_params);
        $i = 0;
        $return = array();
        
        if (empty($hb_params[EMAIL])) {
            return $return;
        }
            
        $return = $this->getMappingParams($params, $hb_params, $hb_contact);

        return $return;
    }

    
    private function getMappingParams($params, $hb_mapping, $hb_params)
    {
        $return = array();
        $i = 0;
        $val = "";
        
        if (empty($params) && empty($hb_mapping) && empty($hb_params)) {
            return false;
        }

        $hb_parm_val = array_values($hb_params);
        $hb_parm_key = array_keys($hb_params);
        $hb_map_val = array_values($hb_mapping);
        
//        var_dump("hb_parm_val", $hb_parm_val);
//        var_dump("hb_parm_key", $hb_parm_key);
//        var_dump("hb_map_val", $hb_map_val);

        
        while ($i < count($hb_parm_key)) {
            #Searches the hubspot mapping with HTML value.
            $val = array_search($hb_parm_val[$i], $hb_map_val);
           
            if (($val == 0 && $i == 0) || $val > 0) {
                # value is present, considers the HTML value
                $return[$hb_parm_key[$i]] = $params[$hb_map_val[$val]];
            } else {
                # value not present, considers the value as constant
                $return[$hb_parm_key[$i]] = $hb_parm_val[$i];
            }
            
            $i++;
        }
        return $return;
    }
    
    
    /**
    * @Function  getCompanyParams.
    *
    * @Description Maps the Company Parameters required to create a Company.
    *
    * @return array of Company parameters.
    *
    */
    private function getCompanyParams()
    {
        $return = false;
        $hb_params = $this->config_hubspot[Company];
        
        if (empty($hb_params['name']) || empty($hb_params['domain'])) {
            return $return;
        }
        $return = array(
            'name' => $hb_params['name'],
            'domain' => $hb_params['domain'],
            'description' => $hb_params['description'],
            'phone' => $hb_params['phone'],
            'zip' => $hb_params['zip'],
        );
        return $return;
    }

    /**
    * @Function  getCompanySearchParams.
    *
    * @Description Maps the Company search Parameters required to search the existence of company.
    *
    * @return array of Company parameters.
    *
    */
    private function getCompanySearchParams()
    {
        return array(
            'limit' => 5,
            'requestOptions' => array(
                'properties' => array(
                    0 => 'domain',
                    1 => 'createdate',
                    1 => 'name',
                    2 => 'hs_lastmodifieddate',
                ),
            ),
            'offset' => array(
                'isPrimary' => true,
                'companyId' => 0,
            ),
        );
    }

    /**
    * @Function  getDealParams.
    *
    * @Description Maps the Deal Parameters required to create a deal.
    *
    * @return array of deal parameters.
    *
    */
    private function getDealParams($param)
    {
        $deal = array();
        $hb_deal = $this->config_hubspot[Deal];
        $hb_mapping = $this->config_mailinone[Mapping];
     
        
        if (empty($hb_deal["dealname"])) {
            return $deal;
        }
        
        $deal = $this->getMappingParams($param, $hb_mapping, $hb_deal);
                
        # Customize the name of the deal
        $deal["dealname"] = $hb_deal["dealname"] .' '. $param[$hb_mapping["FIRSTNAME"]] .' '. $param[$hb_mapping["LASTNAME"]] .' '. $param[$hb_mapping["CITY"]] .' '.  date("Y-m-d");
        
        #var_dump("Deal", $deal);
        
        return $deal;
    }

    
    /**
    * @Function  getAllProperties.
    *
    * @Description Get the properties of contact.
    *
    * @return array of contact propertics.
    *
    */
    private function getAllProperties()
    {
        $hb_prop = new HubSpot_Properties($this->key);
        
        if (empty($hb_prop)) {
            return false;
        }
        
        $prop_objs = $hb_prop->get_all_properties();
        
        foreach ($prop_objs as $prop) {
            array_push($this->properties, $prop->name);
        }
    }
    
    
    
    
    private function checkProperty($field)
    {
        if (empty($field) && empty($this->properties)) {
            return false;
        }
        
        if (array_search($field, $this->properties)) {
            return true;
        }
        
        return false;
    }
    
    /**
    * @Function  getDealAssociations.
    *
    * @Description Maps the Deal Association Parameters required to create a deal.
    *
    * @return array of deal associations parameters.
    *
    */
    private function getDealAssociations()
    {
        $return = false;
        
        if (!isset($this->contact_id)) {
            return false;
        }
        
        $return = array(
            "associations" => array(
                "associatedVids" => array(
                    0 => $this->contact_id
                ),
            ),
        );
        return $return;
    }

    /**
    * @Function  getCompanyDomain.
    *
    * @Description get the domain of the email addresse.
    *
    * @return domain name of email.
    *
    */
    private function getCompanyDomain($param)
    {
        return substr(strrchr($param['email'], "@"), 1);
    }


    /**
    * @Function  createContact.
    *
    * @Description Creates the contact in Hubspot
    *
    * @return contact id of Hubspot.
     *
    *
    */
    public function createContact($params)
    {
        $contact_parms = $this->getContactParams($params);

        try {
            if (empty($contact_parms)) {
                throw new Exception("Hubspot : Invalid Contact Parameters" .PHP_EOL);
            }
            
            $contact = new HubSpot_Contacts($this->key);
            $contact_exists = $contact->get_contact_by_email($contact_parms["email"]);
           
            if (isset($contact_exists->vid)) {
                $this->contact_id = $contact_exists->vid;                                        //  Contact exist in Hubspot
                $contact->update_contact($this->contact_id, $contact_parms);                     //  Does not return anything on update
            } else {
                $this->contact_id = $contact->create_contact($contact_parms);
 
                $this->contact_id = $this->contact_id->vid;
            }

            if (($this->contact_id->status === "error") || ($updated_contact->status === "error")) {
                throw new Exception("Hubspot Contact: ". $this->message .PHP_EOL);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this->contact_id;
    }

    /**
    * @Function  createCompany.
    *
    * @Description Creates the Company in Hubspot
    *
    * @return company id of Hubspot.
    *
    */
    public function createCompany($params)
    {
        $company_parms = $this->getCompanyParams($params);
        try {
            if (!$company_parms) {
                throw new Exception("Hubspot : Invalid Company Parameters".PHP_EOL);
            }

            $company = new HubSpot_Company($this->key);
            $company_exits = $company->get_company_by_domain($params["domain"], $this->getCompanySearchParams());

            if (count($company_exits->results) == 0) {
                $this->company_id = $company->create_company($company_parms);   // Company doesnot exists in Hubspot

                if ($this->company_id->status === "error") {
                    throw new Exception("Hubspot : ". $this->message .PHP_EOL);
                }

                $this->company_id = $this->company_id->companyId;                // Company is created
            } else {
                foreach ($company_exits->results as $domain) {                    // Company exits in Hubspot
                    $this->company_id = $domain->companyId;
                }
            }
            return $this->company_id;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
    * @Function  createDeal.
    *
    * @Description Creates the Deal in Hubspot
    *
    * @return deal id of Hubspot.
    *
    */
 
    public function createDeal($params)
    {
        $deal_parms = $this->getDealParams($params);
        try {
            if (empty($deal_parms)) {
                throw new Exception("Hubspot : Invalid Deal Parameters".PHP_EOL);
            }
            $recent_deals_url = "https://api.hubapi.com/deals/v1/deal/recent/created?hapikey=".$this->key;
            $recentDeals = $this->get_curl($recent_deals_url)->results;                  // Get all the recent Deal
            
            foreach ($recentDeals as $deals) {
                if ($deals->properties->dealname->value === $deal_parms['dealname']) {
                    $this->deal_id = $deals->dealId;
                }
            }
            $deal = new HubSpot_Deal($this->key);
            if (!isset($this->deal_id)) {
                $this->deal_id = $deal->create_deal($this->getDealAssociations(), $deal_parms);   // Associate contact and company with new Deal
                $status = json_decode($this->deal_id)->status;
                if ($status === "error") {
                    throw new Exception("Hubspot :" . $this->message .PHP_EOL);
                }
                $this->deal_id = json_decode($this->deal_id)->dealId;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this->deal_id;
    }
    
    public function get_curl($url)
    {
        if (empty($this->key)) {
            return false;
        }
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ));
        $resp = curl_exec($curl);
        curl_close($curl);

        return (json_decode($resp));
    }
    
    
    /**
    * @Function  associateDealWithContact.
    *
    * @Description Associate Deal with Contact in hubspot
    *
    * @return void.
    *
    */
    
    public function associateDealWithContact($params)
    {
        try {
            if (!isset($params['HubspotContactID']) || !isset($params['HubspotDealID'])) {
                throw new Exception("Hubspot : Invalid Deal Id or Contact Id ".PHP_EOL);
            }

            $url = 'https://api.hubapi.com/deal/'.$params['HubspotDealID'].'/associations/CONTACT?id='.$params['HubspotContactID'].'&hapikey='.$this->key;
            $resp = $this->get_curl($url);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function handleError($error_msg)
    {
        $this->Error = true;
        $this->message = $error_msg;
        throw new Exception("Hubspot error: ". $this->message .PHP_EOL);
        return false;
    }
}
