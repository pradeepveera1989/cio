<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class forwardMail {

    function __construct() {
     
        $this->config = new config("config.ini");
        $this->config_mail = $this->config->getConfigEmail();
    }
    
    public function getMailParams($params){
        
        if(empty($params) || empty($this->config_mail) || empty(trim($this->config_mail[ForwardEmailRecipient]))){
            
            return [];
        }
        $this->config_mailon = $this->config->getConfigMailInOne()[Mapping];
        
        return [
            "to" => $this->config_mail[ForwardEmailRecipient],
            "from" => "admin@campaign-in-one.de",
            "anrede" => $params[$this->config_mailon[SALUTATION]],
            "vorname" => $params[$this->config_mailon[FIRSTNAME]],
            "nachname" => $params[$this->config_mailon[LASTNAME]],
            "telefon" => $params[$this->config_mailon[Telefon]],
            "email" => $params[$this->config_mailon[EMAIL]],
            "subject" => $this->config_mail[ForwardEmailSubject]
        ];
    }
    
    public function getMailMessage(){
        
        return $this->mail_params["subject"] . "\n"
            . "Anrede:" . $this->mail_params["anrede"] . "\n"
            . "Name:" . $this->mail_params["nachname"] . "\n"
            . "Vorname:" . $this->mail_params["vorname"] . "\n"
            . "Telefon:" . $this->mail_params["telefon"] . "\n"
            . "Email:" . $this->mail_params["email"] . "\n";   
    }
    
    public function forwardEmail($params){
        
        $this->mail_params = $this->getMailParams($params);
        if(empty($this->mail_params)){
            
            return false;
        }
        
        $this->mail_msg = $this->getMailMessage();
        
        $this->headers = "From:" . $this->mail_params["from"];
        
        $result = mail(
                $this->mail_params["to"], 
                $this->mail_params["subject"], 
                $this->mail_msg, 
                $this->headers);   
        
        return true;
    }
}
