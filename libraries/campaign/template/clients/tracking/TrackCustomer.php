<?php

/**
 * TrackCustomer
 *
 * Trackes the Customer Journey in the form of Touch points.
 * URLs based on active webinstance.
 * php version 7.2.10
 *
 * @category  TrackCustomer
 * @package   TrackCustomer
 * @author    Pradeep Veera <pradeep.veera@treaction.net>
 * @copyright 2015 treaction.net
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://treaction.net
 * @since     1.4.0
 */

class TrackCustomer
{
    public $ts = ''; // Traffic Source.
    public $config = '' ; // Config.
    public $table;

    /**
     * __construct function
     */
    public function __construct()
    {
        $this->config = new config('config.ini');
        $this->table = "`cio.customer_journey`";
        $this->dbObject = new db();
    }

    /**
     * Function createTouchPoint
     *
     *  - If is one to many touchpoint last one is always stored directly in DB
     *  - If there are more one touchpoint add all of them to cio_customer_journey
     * @param string $ts      customer touchpoint.
     * @param string $ref_url refferer URL
     * @return bool
     */
    public function createTouchPoint(string $ts = null, string $ref_url = null) : bool
    {
        $status = false;

        // Check for current time stamp
        if (!$this->getCurrentTimeStamp()) {
            return $status;
        }

        // Get the traffic source either from $ts or $ref_url.
        if (!$this->getTrafficSource($ts)) {
            if (!$this->getRefererFromURL($ref_url)) {
                return $status;
            }
        }

        // Concatinating traffic source with time stamp
        $tp = $this->ts .' - '.$this->time_stamp;
        $tp_count = $this->getCountOfTouchPoints();

        // Validate Touchpoint
        if (!$this->validateTouchPoint()) {
            // Modifying the above touchpoint.
            $tp_count = $tp_count -1;
        }

        // setcookie returs boolean
        $status = setcookie("cookie[touch_point][$tp_count]", $tp, strtotime('+30 days'), "/");
        return $status;
    }

    /**
     * Parse URL and returns the Referer
     *
     * @param string $ref_url Parse the URL
     * @return bool
     */
    private function getRefererFromURL(string $ref_url = null) :bool
    {
        $status = false;
        if (empty($ref_url) || !filter_var($ref_url, FILTER_VALIDATE_URL)) {
            return $status;
        }

        // Parse URL to get Host name.
        $parse_url = parse_url($ref_url);
        $host = $parse_url["host"];
        $parse_host = explode('.', $host);

        // Exclude '.com' or '.de' or '.net' from Hostname.
        if (is_array($parse_host) && isset($parse_host[0]) && !empty($parse_host[0])) {
            $this->ts = $parse_host[0];
            $status = true;
        }

        return $status;
    }

    /**
     * Validates the Touch Point function
     *
     * @return boolean
     */
    private function validateTouchPoint() :bool
    {
        $status = false;
        if (!isset($this->ts) || empty($this->ts)) {
            return $status;
        }
        $ts = $this->ts;
        $tps = $this->getAllTouchPoints();
        $tp_count = $this->getCountOfTouchPoints();

        if (is_numeric($tp_count) && $tp_count == 0) {
            // First TouchPoint
            $status = true;
        } else {
   
            // Not the First touchpoint
            // Compare with last touchpoint.
            $last_tp = $tps[$tp_count-1];
            $last_ts = $this->getTrafficSourceFromTouchPoint($last_tp);
            
            // status will be true if the ts is not similar to last_ts
            $status = ($ts === $last_ts) ? false : true;
        }
        return $status;
    }


    /**
     * Gets the Traffic source from the given trouch point
     *
     * @param  string $tp touch point
     * @return string
     */
    private function getTrafficSourceFromTouchPoint(string $tp) :string
    {
        $ts = "";
        if (!isset($tp) || empty($tp)) {
            return $ts;
        }

        $tmp = explode(' - ', $tp);
        if (is_array($tmp) && !empty($tmp)) {
            $ts = $tmp[0];
        }

        return $ts;
    }


    /**
     * Counts the number of touch point currently Campaign holds
     *
     * @return int number of touchpoints
     */
    private function getCountOfTouchPoints() :int
    {
        $tp_count = 0;
        $tps = $this->getAllTouchPoints();
        $count = count($tps);
        if (is_numeric($count) && (int) $count >= 0) {
            $tp_count = $count;
        }

        return $tp_count;
    }


    /**
     * Returns all the trouch points
     * @return array touch point array
     */
    private function getAllTouchPoints() :array
    {
        $t_points = [];
        if (is_array($_COOKIE["cookie"]["touch_point"]) && !empty($_COOKIE["cookie"]["touch_point"])) {
            $t_points = $_COOKIE["cookie"]["touch_point"];
        }

        return $t_points;
    }


    /**
     * Get the trafficsource  
     * @param string $ts traffic source
     * @return bool
     */
    private function getTrafficSource(string $ts=null) : bool
    {
        $status = false;

        if (!empty($ts) && isset($ts)) {
            $this->ts = $ts;
            $status = true;
        }

        return $status;
    }


    /**
     * Returns the current timestamp and set to 
     * the value to $this->time_stamp.
     *
     * @return boolean
     */
    private function getCurrentTimeStamp() : bool
    {
        $status = false;
        $t_stamp = date("Y-m-d h:i:s", time());

        if (isset($t_stamp) && !empty($t_stamp)) {
            $this->time_stamp = $t_stamp;
            $status = true;
        }
    
        return $status;
    }


    /**
     * Inserts the value of Touchpoint to Database.
     *
     * @param  int $lead_id Lead Id from DB.
     * @return void
     */
    public function insertTouchPointsToDB(int $lead_id)
    {
        $status = false;
       
        if (empty($lead_id) || !isset($lead_id) || !isset($this->dbObject) || !isset($this->table)) {
            return $status;
        }
        // Get all the Touchpoints
        $tps = $this->getAllTouchPoints();
        if (!is_array($tps) || empty($tps)) {
            return $status;
        }

        $status = true;
        foreach ($tps as $tp) {
            // bypass empty string
            if (!isset($tp) || empty($tp)) {
                continue;
            }
            $temp = explode(' - ', $tp);
            $touchpt = $temp[0];
            $touchpt_timestamp = $temp[1];

            $value = [
                'lead_id'   =>  $lead_id,
                'touchpoint' => $touchpt,
                'touchpoint_timestamp' => $touchpt_timestamp,
                'created'   =>  date("Y-m-d h:i:s", time()),
                'updated'   => date("Y-m-d h:i:s", time())
            ];
    
            $operation = "insert";
            // Insert touchpoint to DB
            $result = $this->dbObject->execute($this->table, $value, "", $operation);
            if (is_bool($result) && $result == false) {
                $status = false;
            }
        }

        return $status;
    }
}
?>