<?php

/*
 *
 *  Fackbook Pixel Tracking
 * 
 * 
*/
?>



<?php
# Facebook Pixel URL
$facebookpixel = $config[TrackingTools][FacebookPixel];
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";
?>

<script>

    var fb_events = "<?= $config[TrackingTools][FacebookEvent] ?>";
    var fb_event = (fb_events.split(",")); // Split the events to array
    var fb_curreny = "<?= $config[TrackingTools][FacebookCurrency] ?>";
    var fb_value = "<?= $config[TrackingTools][FacebookValue] ?>";

    /*Get the facebook pixel code from config.ini */
    var facebookpixel = <?= $facebookpixel ?>;
    !function (f, b, e, v, n, t, s)
    {
        if (f.fbq)
            return;
        n = f.fbq = function () {
            n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)
            f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', <?= $facebookpixel ?>);
    fbq('track', 'PageView');

    fb_event.forEach(function (e) {
        e = e.trim();
        // Purchace event needs currency and amount value 
        if (e == "Purchase") {
            fbq('track', e, {currency: fb_curreny, value: fb_value});
        } else {
            fbq('track', e);
        }
    })

</script>
<noscript>
<img height="1" width="1" style="display:none" src= "<?php echo $fbURL ?>">
</noscript>	