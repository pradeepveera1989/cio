<?php

require "vendor/autoload.php"; 
require "libraries/autoload.php";

session_start();
// Get the Config file path
$config_path = "config.ini";
$conf = new config($config_path);
$config = $conf->getAllConfig();
var_dump("Test");
spl_autoload_register('Autoload::generalFunctionLoader');
var_dump("Test");
$func = new generalFunctions();
var_dump($func);
$func->getUTMParsamFromURL();
die();

if(!empty($_GET)){
    foreach($_GET as $key => $value){
        $_SESSION[$key] = $value;
    }
}



// Page counter
$counter_status = $conf->getConfigCounterStatus();
if($counter_status) {

    $counter = new counter();
    $url = "https://".$_SERVER[HTTP_HOST]."/".explode('/', $_SERVER['REQUEST_URI'])[1];
    $current_count_value = $counter->getCounterValue($url);
}

//Split Test
if($conf->getConfigSplitTestStatus()) {
    
    spl_autoload_register('Autoload::splitTestLoader');
    $currentURL = getcwd();
    $spTest = new splitTestClient($config[Splittest][Name], $config[Splittest][NumberOfVariants]);
    $redirect_url = $spTest->getRedirectURL($currentURL);
    
    // Redirect the Page
    echo '<script type="text/javascript">';
	echo "window.top.location='".$redirect_url."'";
    echo '</script>';     
    
}

// Post Request
if(isset($_POST[$config[MailInOne][Mapping][EMAIL]])){
    
    $params = array(); 

    // Email Validation
    $validate = new emailValidatorApiClient($_POST[$config[MailInOne][Mapping][EMAIL]]);
    $validate->curlRequest();
    $resp = $validate->curlResponce();

    if (!$resp) {
        // invalid email address
        // Copying the data to session and intiating a sesssion variable error.
        $_SESSION['data'] = $_POST;
        $_SESSION['error'] = "email";
        
        echo '<script type="text/javascript">';
        echo 'window.location ="#form"';
        echo '</script>';
    }else {
        
        $_SESSION['error'] ="";
        
        // Map the POST params to Maileon Params
        $maileon_params = $config[MailInOne][Mapping];
        foreach ($maileon_params as $mp){
            
            $params[$mp] = $_POST[$mp];
        } 

        $params = $conf->checkUTMParamsInPOST($params);
        var_dump("parameters", $params);
        die();        
        // Get the date 
        $params[$config[MailInOne][Mapping][Datum]] = date('d.m.Y \\u\\m h:i:s a', time());
        $params[$config[MailInOne][Mapping][ip_adresse]]  = $_SERVER['REMOTE_ADDR']; 
        $params["url_params"] = parse_url($params[$config[MailInOne][Mapping][url]])["query"];
        
        // Session
        foreach ($_SESSION as $key=>$value){
            
            $params[$key] = $value;
        }

        // Set a cookie
        $cookie = $params[$config[MailInOne][Mapping][EMAIL]];
        setcookie("campaign", $cookie);        
        
        // Keep URL Parameters
        $success_page = $config[GeneralSetting][SuccessPage];
        
        if($conf->getConfigKeepURLParms()){                      
            $success_page.= '?'.$params["url_params"];
        }
 
        // Update the Counter Value
        $new_counter_value = $current_count_value - 1;
        $counter->updateCounterValue($url, $new_counter_value);        
        
        // Assignment Manager
        if($conf->getConfigAssignmentManagerStatus()){   
           
            $assignmentmanager = new assignmentManager();
            $vertriebsmitarbeiter = $assignmentmanager->getAssignmentManager(
                                                            $params[$config[MailInOne][Mapping][ZIP]], 
                                                            $config[AssignmentManager][Vertriebsmitarbeiter]
                                                         );
            $vertriebsmitarbeiterid = $assignmentmanager->getAssignmentManagerId();
            $params[$config[MailInOne][Mapping][Vertriebsmitarbeiter]] = $vertriebsmitarbeiter;
            $params[$config[MailInOne][Mapping][Vertriebsmitarbeiterid]] = $vertriebsmitarbeiterid;        
        }    
        
        
        //Hubspot Configuration Sync
        if($conf->getConfigHubspotStatus()){

            spl_autoload_register('Autoload::hubspotLoader');
            $hubspot = new hubspotApiClient();
            $params[$config[MailInOne][Mapping][HubspotContactID]] = $hubspot->createContact($params);
            $params[$config[MailInOne][Mapping][HubspotDealID]] = $hubspot->createDeal($params);
            $hubspot->associateDealWithContact($params);
        }

        // Forward Email
        if($conf->getConfigForwardEmail()){
            
            $mail = new forwardMail();
            $s = $mail->forwardEmail($params);
        }
        
        // Maileon Configuration Sync
        if ($conf->getConfigMailInOneStatus()) {        
            
            spl_autoload_register('Autoload::maileonLoader');
            $maileon = new maileonApiClient();
            $response = $maileon->insertContact($params);
            $status = $maileon->intiateMarketingAutomation();

            // Redirect the Page
            echo '<script type="text/javascript">';
			echo "window.top.location='".$success_page."'";
            echo '</script>'; 
        }
    } 
}    

	# Facebook Pixel URL
	$facebookpixel = $config[TrackingTools][FacebookPixel];
	$fbURL = "https://www.facebook.com/tr?id=".$config[TrackingTools][FacebookPixel]."&ev=PageView&noscript=1";
	
	# Google Maps URL
	$googleKey = $config[TrackingTools][GoogleAPIKey];
	$googleURL = "https://maps.googleapis.com/maps/api/js?key=".$config[TrackingTools][GoogleAPIKey]."&libraries=places";

	#Google Analytics Key
	$googleAnalyticsKey = $config[TrackingTools][GoogleAnalytics];
	$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=".$config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
    <meta name="author" content="StimaWELL">
      
    <title>Das neue StimaWELL-EMS eBook</title>
      
    <link href="css/creative.css" rel="stylesheet" type="text/css">  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
	<link rel="stylesheet" href="libraries/telefonvalidator-client/build/css/intlTelInput.css"/>
    <link rel="stylesheet" href="css/animate.css"/>
  </head>

    <script type="text/javascript" src="<?php echo $googleURL; ?>"></script>   	
	<script async src="<?php echo $googleAnalyticsURL;?>"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', '<?php echo $googleAnalyticsKey; ?>');
	</script>	
	<script>
	  /*Get the facebook pixel code from config.ini */
		var facebookpixel = <?=$facebookpixel ?>;
		!function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
	      s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', <?=$facebookpixel ?> );
		  fbq('track', 'PageView'); 
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src= "<?php echo $fbURL ?>"/>
	</noscript>	

   <script> 
            // Set the date we're counting down to
        var countDownDate = new Date("<?= $config[GeneralSetting][CountdownExpire];?>").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

          // Get todays date and time
          var now = new Date().getTime();

          // Find the distance between now an the count down date
          var distance = countDownDate - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // Display the result in the element with id="demo"
          document.getElementById("demo").innerHTML = days + " Tage " + hours + " Stunden "
          + minutes + " Min " + seconds + " Sek ";

          // If the count down is finished, write some text 
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "<?= $config[GeneralSetting][CountdownExpireMessage];?>";
          }
        }, 1000); 
</script>

    
  <body>

    <!-- COUNTDOWN -->
    <section>
                 <div class="nav-bottom">
                   <div class="container">
                       <div class="row">
                         <div class="col-sm-12 text-center">

                             <span class="countdown-nav">Aktionsende:&nbsp;&nbsp;</span><span class="countdown-nav" id="demo"></span>&nbsp;<span class="countdown-nav">-&nbsp;&nbsp;Nur noch <?php echo $current_count_value; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>
                             

                          </div> 
                       </div>
                    </div>   
                </div>  

    </section>   
    <!-- ./COUNTDOWN -->            
      
    <!-- Contact Formular -->  
    <section class="form-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 check-submit-form" id="form" style="padding-top: 50px; padding-bottom: 20px;">     
                    <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                        <input type="hidden" name="contactid" value="<?= $contactid ?>">
                        <input type="hidden" name="checksum" value="<?= $checksum ?>">
                        <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                        <!-- INPUT ANREDE FRAU & HERR -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="btn-group">
                                    <?php
                                    if ($_SESSION['data']['anrede'] === "Frau") {
                                        $frau = "checked";
                                    } else {
                                        $herr = "checked";
                                    }
                                    ?>										
                                    <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                        <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" required<?php echo $frau; ?>> Frau
                                    </label>
                                    <span class="input-group-btn" style="width:15px;"></span> 
                                    <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                        <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr ?>> 
                                        Herr
                                    </label>
                                </div>  
                            </div>       
                        </div>

                        <!-- INPUT VORNAME -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">

                                    <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data']['vorname'])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                           <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                </div>
                            </div>       
                        </div>


                        <!-- INPUT NACHNAME -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">

                                    <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data']['nachname'])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                            <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                </div>
                            </div>       
                        </div>


                        <!-- INPUT PLZ -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data']['plz'])); ?>"  class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="number" required 
                                           <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                </div>
                                <span class="p-light errorplz" style="color:<?php echo $config[Postal_Code][ErrorMsg_Color]; ?>"><?php echo $config[Postal_Code][ErrorMsg]; ?></span>     
                            </div>       
                        </div>

                        <!-- INPUT ORT -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input value="<?php echo ((!empty($standard_005) ? $standard_005 : $_SESSION['data']['ort'])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text" required  
                                           <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                </div>
                            </div>       
                        </div>

                        <!-- INPUT EMAIL -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data']['email'])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                           <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                </div>
                                <?php if ($_SESSION['error'] === "email") { ?>   
                                    <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
<?php } ?>  										
                            </div>       
                        </div>

                        <!-- INPUT TELEFON -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >

                                           <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   

                                </div>
                                <span class="p-light errortelefon" style="color:<?php echo $config[Telefon][ErrorMsg_Color]; ?> ;float:right"><?php echo $config[Telefon][ErrorMsg]; ?></span>
                            </div>       
                        </div>                                

                        <label class="form-group">
                            <div class="col-sm-12">
                                <input style="width: 20px; height: 20px;" type="checkbox" required>
                                <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de">widerruf@schwa-medico.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                            </div>    
                        </label>
                        <!--Hidden Fields-->   
                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">URL</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                            </div>
                        </div>                                        

                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">UTM Source</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source']?>">
                            </div>
                        </div>  

                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">UTM Name</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name']?>">
                            </div>
                        </div>      
                        
                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">UTM Term</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term']?>">
                            </div>
                        </div>                             
                        
                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">UTM Content</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content']?>">
                            </div>
                        </div>                              
                        
                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">UTM Medium</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium']?>">
                            </div>
                        </div>                            
                                            
                        
                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                            <div class="col-sm-10">
                                <input class="form-control"  value="<?= $ts ?>" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource']?>">
                            </div>
                        </div>

                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">Quelle</label>
                            <div class="col-sm-10">
                                <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                            </div>
                        </div>

                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">Typ</label>
                            <div class="col-sm-10">
                                <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][typ]; ?>">
                            </div>
                        </div>

                        <div class="form-group hidden">
                            <label class="col-sm-2 control-label" for="">Segment</label>
                            <div class="col-sm-10">
                                <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                            </div>
                        </div>


                        <div class="form-group"> 
                            <div class="col-sm-12">
                                <input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="SUBMIT" name="submit" style="width: 100%;">
                            </div>        
                        </div>
<?php if (isset($response) && $response->isSuccess()) { ?>
                            <div class="alert alert-success fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Subscription successful</strong>
                            </div>
<?php } elseif (isset($warning)) { ?>
                            <div class="alert alert-warning fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                            <?= $warning['message'] ?>
                            </div>
<?php } elseif (isset($response)) { ?>
                            <div class="alert alert-danger fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Subscription failed</strong>
                            </div>
<?php } ?>
                    </form>          


                </div>
            </div>
        </div>  
    </section>
	  

    <!-- FOOTER SECTION -->  
      
      
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="libraries/telefonvalidator-client/build/js/utils.js"></script>		
	<script type="text/javascript" src="libraries/telefonvalidator-client/build/js/intlTelInput.js"></script>	
    <script src="vendor/google/maps.js"></script>  
    <script type="text/javascript" src="libraries/jquery-cookie-master/src/jquery.cookie.js"></script>  
    <!--script src="libraries/ini-master/ini.js"></script-->  
    <!--script src="jquery/campaign.js"></script-->  
       
      
    <script>
      
        $('.close').click(function () {
            $('#modal-video').hide();
            $('#modal-video iframe').attr("src", jQuery("#modal-video iframe").attr("src"));
        });  
    </script>

      
            
    <script>
    
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();

            if (scroll > 660) {
                $(".nav-bottom").addClass("change"); // you don't need to add a "." in before your class name
            } else {
                $(".nav-bottom").removeClass("change");
            }
        });
    </script>

            
            
            
            
    <script type="text/javascript ">

        
        $(document ).ready(function() {
            var countdown_status = "<?=$counter_status?>";
            var current_stocks = "<?= $current_count_value;?>";            
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
            var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage]?>";
			var validate_telefon = "<?= $config[Telefon][Status];?>";
            var cookie = "<?=$cookie;?>";

			// telefon validation 
			var settings = {
				utilsScript: "libraries/telefonvalidator-php-client/build/js/utils.js",
				preferredCountries: ['de'],			
				onlyCountries:['de','ch','at'],
			};		
			$('#telefon').intlTelInput(settings);			

			// Error messages
            $('.errorplz').hide();
			$('.errortelefon').hide();
            
            //Cookie 
            if($.cookie('campaign')) {
                disableContactForm("User with this email address have already submitted the form");
            }   
            
            // Countdown counter
            if(countdown_status != 1 || current_stocks <=0) {
                disableContactForm(countdown_expire_message);
            }   
 
           function disableContactForm(err_msg){
               
                $("input.input-fields").prop('disabled', true);
                $("input[type=radio]").prop('disabled', true);
                $("input[type=checkbox]").prop('disabled', true);
                $( "span.countdown-nav").css('display','none');
                $( "span.countdown-nav:last-child" ).replaceWith( "<span style='color:#fff;font-size:16;text-transform:uppercase'>" + err_msg + "</span>");                
            }   
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                console.log(zip);
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }
			
			$('#telefon').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon'); 
                tel2 = $('#tel2');
				telnum = tel.val();				
				var error = false;
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
				// Check for alphabits and special characters from regex expresssion
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}			
                    if(telnum.charAt(0) == 0){
                        console.log("Inside");
                        telnum = telnum.substring(1);
                    }
                   
                    console.log("After removiing", telnum)
				
                // Get the Conuntry code     
                    var ext = tel.intlTelInput("getSelectedCountryData").dialCode;
                    console.log("extention", ext);
                    var num = "+" + ext + ' ' + telnum;
                    console.log("num", num);
                    tel2.val(num);
                    
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});					
			
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
				if(validate_telefon) {
					if($('.errortelefon').css('display') !== 'none'){
						return false;
					}
				}	                
			
				form.find("input[type='submit']").prop('disabled', true);
            }); 
        });     
       </script>	
  </body>
</html>