<?php

class db
{
    private $conn;
    private $host;
    private $user;
    private $password;
    private $baseName;
    private $query;
    
    public function __construct()
    {
        $this->host = "localhost";
        $this->user = "db13026946-cio";
        $this->password  = "6gRjTSD5";
        $this->dbName = "db13026946-cio";
        $this->conn = false;
        $this->query = "";
        $this->connect();
    }
    
    private function connect()
    {
        if (!$this->conn) {
            $this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->dbName);
          
            if (!$this->conn) {
                $this->status_fatal = true;
                echo 'Connection BDD failed';
                die();
            } else {
                $this->status_fatal = false;
            }
        }
        return $this->conn;
    }
    
    public function disconnect()
    {
        if ($this->conn) {
            @pg_close($this->conn);
        }
    }
    
    /**
     * getSelectQueryStatement function
     *
     * @param array $select
     * @param string $table
     * @param array $where
     * @return void
     */
    private function getSelectQueryStatement(array $select, string $table, string $where) :void
    {
        if (isset($select) && isset($table)) {
            $select = (count($select) > 1)? implode(", ", $select) : implode($select);
            $this->query = "select ". $select . " from " . $table . " where ". $where;
        } else {
            $this->query = "";
        }
    }
    
    /**
     * getOne function
     * Retrives single row from DB
     *
     * @param array $select
     * @param string $table
     * @param array $where
     * @return array
     */
    public function getOne(array $select, string $table, string $where): array
    {
        $cnx = $this->conn;
        $return = [];
        if (!$cnx || $this->status_fatal) {
            echo 'GetOne -> Connection BDD failed';
            die();
        }
        
        $this->getSelectQueryStatement($select, $table, $where);
        $cur = mysqli_query($cnx, $this->query);
        if ($cur == false) {
            $errorMessage = mysqli_error($cnx);
            $this->handleError($query, $errorMessage);
        } else {
            $this->Error=false;
            $this->BadQuery="";
            $tmp = mysqli_fetch_array($cur, MYSQLI_ASSOC);
    
            if (is_array($tmp) && !empty($tmp)) {
                $return = $tmp;
            }
        }
        @mysqli_free_result($cur);

        return $return;
    }


    /**
     * getAll function
     * when you need to select more than 1 line in the database
     *
     * @param string $query
     * @return array
     */
    public function getAll(string $query) : array
    {
        // getAll function: when you need to select more than 1 line in the database
        $return = array();
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'GetAll -> Connection BDD failed';
            die();
        }
        // For German Characters.
        mysqli_set_charset($cnx, "utf8");
        $cur = mysqli_query($cnx, $query);
        
        while ($data = mysqli_fetch_assoc($cur)) {
            array_push($return, $data);
        }

        return $return;
    }
    
    
    /**
     * getInsertQueryStatement function
     *
     * @param string $table
     * @param array $value
     * @return void
     */
    public function getInsertQueryStatement(string $table, array $value):void
    {
        $val = array();
        $col = array();
        if (isset($table) && isset($value)) {
            foreach ($value as $key => $value) {
                array_push($col, $key);
                array_push($val, "'".$value."'");
            }
          
            $val = (count($val) > 1)? implode(", ", $val) : implode($val);
            $col = (count($col) > 1)? implode(", ", $col) : implode($col);
            $this->query = "INSERT INTO ".$table." (" . $col . ") VALUES (".$val.")";
        }
    }
    
    private function getUpdateQueryStatement($table, $value, $where)
    {
        if (isset($table) && isset($value)) {
            foreach ($value as $key => $value) {
                $val = $key ." = ". $value;
            }

            foreach ($where as $key => $value) {
                array_push($where, $key ." = ". $value);
            }
            
            $this->query = "UPDATE ".$table. " SET " .$val. " WHERE " .$where;
        } else {
            $this->query = false;
        }
    }
    
    /**
     * execute function
     * INSERT or UPDATE Query
     *
     * @param string $table
     * @param array $value
     * @param array $where
     * @param string $operation
     * @param boolean $use_slave
     *
     * @return void
     */
    public function execute(string $table, array $value, string $where, string $operation, $use_slave = false)
    {
        $cnx = $this->conn;

        if (!$cnx||$this->status_fatal) {
            return null;
        }
        if ($operation === "update") {
            $this->getUpdateQueryStatement($table, $value, $where);
        } elseif ($operation === "insert") {
            $this->getInsertQueryStatement($table, $value);
        }
        # Manually set the charcterset to 'UTF-8' encoding
        @mysqli_set_charset($cnx, "utf8");
        $cur = @mysqli_query($cnx, $this->query);
        if ($cur == false) {
            $ErrorMessage = mysqli_error($cnx);
            $this->handleError($query, $ErrorMessage);
        } else {
            $this->Error=false;
            $this->BadQuery="";
            $this->NumRows = mysqli_affected_rows();
            return $cur;
        }

        @mysqli_free_result($cur);
    }

    /**
     * getLastInsertId function
     *
     * @return integer
     */
    public function getLastInsertId():int
    {
        $cnx = $this->conn;
        $last_insert_id = 0;
        if (!$cnx||$this->status_fatal) {
            return null;
        }
        
        $id = mysqli_insert_id($cnx);

        if ((int) $id >= 0  && !empty($id)) {
            $last_insert_id = (int)$id;
        }

        return $last_insert_id;
    }
    
    /**
     * insertMultiRows function
     *
     * @param array $insertQuery
     * @return boolean
     */
    public function insertMultiRows(string $insertQuery): bool
    {
        $cnx = $this->conn;
        $return = false;

        if (!$cnx||$this->status_fatal || empty($insertQuery)) {
            die("insertMultiRows");
            return $return;
        }

        # Manually set the charcterset to 'UTF-8' encoding
        @mysqli_set_charset($cnx, "utf8");
        $cur =  @mysqli_multi_query($cnx, $insertQuery);

        if ($cur == false) {
            $ErrorMessage = @mysqli_last_error($cnx);
            $this->handleError($query, $ErrorMessage);
        } else {
            $this->Error=false;
            $this->BadQuery="";
            $this->NumRows = mysqli_affected_rows();
        }
        
        $return = $cur;
        @mysqli_free_result($cur);

        return $return;
    }
    
    public function handleError($query, $str_erreur)
    {
        $this->Error = true;
        $this->BadQuery = $query;
        if ($this->Debug) {
            echo "Query : ".$query."<br>";
            echo "Error : ".$str_erreur."<br>";
        }
    }
}
