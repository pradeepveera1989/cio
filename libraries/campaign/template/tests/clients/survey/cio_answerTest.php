<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cio_answerTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;

class cio_answerTest extends TestCase{
    //put your code here
    public function setUp() {
        
        $this->cio_answer = new cio_answers();
    }         
    
    public function testinsertSurveyAnswers(){
        
        $this->assertEquals($this->cio_answer->insertSurveyAnswers(array()), 0);
        $this->assertEquals($this->cio_answer->insertSurveyAnswers(['answer'=>" ", "lead_customfield_id" => " "]), 0);
        $this->assertEquals($this->cio_answer->insertSurveyAnswers(['answer'=>"Test answer", "lead_customfield_id" => -2]), 0);
        $this->assertEquals($this->cio_answer->insertSurveyAnswers(['answer'=>"Test", "lead_customfield_id" => 0]), 0);
    }
    
    public function testgetAnswerOptionForQuestionId(){
     
        $this->assertEquals($this->cio_answer->getAnswerOptionForQuestionId(array()), []);
        $this->assertEquals($this->cio_answer->getAnswerOptionForQuestionId(['answer'=>"Test", 'question_id' => 0, 'question_type'=>"Test"]), []);
        $this->assertEquals($this->cio_answer->getAnswerOptionForQuestionId(['answer'=>" ", 'question_id' => 1, 'question_type'=>"Test"]), []);
        $this->assertEquals($this->cio_answer->getAnswerOptionForQuestionId(['answer'=>"Test", 'question_id' => 2, 'question_type'=>" "]), []);
        $this->assertEquals($this->cio_answer->getAnswerOptionForQuestionId(['answer'=>"Test1234", 'question_id' => 0, 'question_type'=>"-3"]), []);
    }
}
