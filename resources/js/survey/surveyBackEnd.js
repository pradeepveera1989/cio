/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    // Hide the Error Message div.
    $('.survey_error').hide();

    $("form").submit(function (e) {
        
       var status = true;
       var s_obj = $('form').getForm2obj();
       var errorMsg = "";       
       var que
       $('.survey_error').hide();
       
       
       // Validating Branching Question Ids
       if(validateBranching(s_obj.survey)){
           status = false;
           errorMsg = "Invalid Branching Question numbers";          
       }
       
       //Validating Image Format
       //Validating Space format
       
       
       if(status !== true && (errorMsg.trim() !== "")){
           e.preventDefault();
           errorAlert(errorMsg)
       }
       

    });

    /*  errorAlert
     * 
     *  Generalized error function for all the jquery
     *  @param errorMsg : Error message is passed to jquery function.
     *  
     *  @return : Display error 
     */
    function errorAlert(errorMsg){
        
        var status = false;
        if(errorMsg.trim() === "") {
            return false
        }
        $('.survey_error').html(errorMsg);
        $('.survey_error').show();
    }

    /*  validateBranching
     * 
     *  Validates Branching question Number 
     *  Error:
     *      1. No question number is existed in survey
     *      2. Question is branched to same question.
     */
    function validateBranching(quesObj){
        
        //Each question should be branched to valid Question id,
        //Each question should not be branched to same question id.
        var ques_ids = getAllQuestionIds(quesObj);
        var status = false;
        for(const i in quesObj.question){
            if(quesObj.question[i].type === "select"){
                var options = quesObj.question[i].select.options;
                var curr_ques_id = parseInt(quesObj.question[i].id);
                for(var opt_id in options){
                    var goto_id = parseInt(options[opt_id].goto) >0 ? parseInt(options[opt_id].goto) : 0;
                    if(( goto_id != 0 && ($.inArray(goto_id, ques_ids) < 0 || (curr_ques_id === goto_id)))) {
                        status = true;
                    }
                }                
            }
        }

        return status
    }
    
    /*  getAllQuestionIds
     * 
     *  Get all the question Ids from survey to an array 
     */
    function getAllQuestionIds(){
        
        var str_ques_ids; 
        var ques_ids_local = getQuestionIdsFromLocalStorage();
        var ques_ids_div = getQuestionIdsFromDiv();
        var ques_ids = $.merge(ques_ids_local, ques_ids_div);

        return ques_ids;
    }
    
    /*  getQuestionIdsFromLocalStorage
     * 
     *  Get all the question Ids Local storage if present
     */    
    function getQuestionIdsFromLocalStorage(){
        
        var str_ques_ids ;
        var ques_ids_local ;
        str_ques_ids = localStorage.getItem("ques-ids");
        ques_ids_local = str_ques_ids.split(',').map(Number);
        
        return ques_ids_local;
    }
    
    /*  getQuestionIdsFromDiv
     * 
     *  Get all the question Ids from div if present
     */     
    function getQuestionIdsFromDiv(){
        
        var ques_ids = [];
        var div_ques_id = $(".ques-id"); 
        div_ques_id.each(function() {
            ques_ids.push(parseInt($(this).val()));
        });         
        
        return ques_ids;
    }
    

   // Convert Form Elements to Js Object    
    (function ($) {
        $.fn.getForm2obj = function () {
            var _ = {}, _t = this;
            this.c = function (k, v) {
                eval("c = typeof " + k + ";");
                if (c == 'undefined')
                    _t.b(k, v);
            }
            this.b = function (k, v, a = 0) {
                if (a)
                    eval(k + ".push(" + v + ");");
                else
                    eval(k + "=" + v + ";");
            };
            $.map(this.serializeArray(), function (n) {
                if (n.name.indexOf('[') > -1) {
                    var keys = n.name.match(/[a-zA-Z0-9_]+|(?=\[\])/g), le = Object.keys(keys).length, tmp = '_';
                    $.map(keys, function (key, i) {
                        if (key == '') {
                            eval("ale = Object.keys(" + tmp + ").length;");
                            if (!ale)
                                _t.b(tmp, '[]');
                            if (le == (i + 1))
                                _t.b(tmp, "'" + n['value'] + "'", 1);
                            else
                                _t.b(tmp += "[" + ale + "]", '{}');
                        } else {
                            _t.c(tmp += "['" + key + "']", '{}');
                            if (le == (i + 1))
                                _t.b(tmp, "'" + n['value'] + "'");
                        }
                    });
                } else
                    _t.b("_['" + n['name'] + "']", "'" + n['value'] + "'");
            });
            return _;
        }
    })(jQuery);
});    
