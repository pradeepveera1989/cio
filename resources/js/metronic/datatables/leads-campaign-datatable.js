"use strict";
var KTDatatablesLeadCampaign = function () {

    var initTableLeadCampaign = function () {
        var lead_status = "";
        // Helper function to generate URL Using Ids
        function getURLForOperation(ids, operation) {

            var url = ""
            var urlObj = window.location;

            if ((ids.length === 0) || (operation.trim() == "") || $.isEmptyObject(urlObj)) {
                return url;
            }

            var host = urlObj.host;
            var protocol = urlObj.protocol;
            var create_url = protocol + '//' + host + '/account/' + ids[0] + '/lead/' + ids[2] + '/' + operation;
            if (isValidURL(create_url)) {
                url = create_url;
            }
            return url;
        }



        // Validates if the String is URL or not
        function isValidURL(string) {
            let res;
            res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
            if (res == null)
                return false;
            else
                return true;
        }



        var table = $('#leads_per_campaing');
        var id = 0;
        // begin first table
        table.DataTable({
            responsive: true,
            // // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            select: {
                style: 'multi'
            },

            // lengthMenu: [5, 10, 25, 50],
            paging:true,
            bInfo : false,
            pageLenght : 10,
            ordering: true,
            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            //order: [[1, 'desc']],

            headerCallback: function (thead, data, start, end, display) {
                console.log("Hello");
                thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input type="checkbox" value="" class="m-group-checkable">
                        <span></span>
                    </label>`;
            },
            columnDefs: [
                {
                    targets: 0,
                    width: '30px',
                    autoWidth: false,
                    className: 'dt-right record-id',
                    orderable: true,
                    render: function (data, type, full, meta) {
                        return `
                             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                 <input type="checkbox" value="` + data + `" class="m-checkable" id="record-id">
                                 <span></span>
                             </label>`;
                    },
                },
                {
                    targets :8,
                    className:'noClick align-middle hide',
                    render: function (data, type, full, meta) {
                        var status = {
                            active: { 'title': 'active', 'class': 'kt-badge--success' },
                            inactive: { 'title': 'inactive', 'class': ' kt-badge--danger' },

                        };

                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        lead_status = status[data].title;
                        return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
                    },
                },
                {
                    targets :9,
                    className:'noClick align-middle hide',
                    visible: false
                },
                {
                    targets :10,
                    className:'noClick align-middle hide',
                    visible: false
                },
                {
                    targets :11,
                    className:'noClick align-middle hide',
                    visible: false
                },
                {
                    targets :12,
                    className:'noClick align-middle hide',
                    visible: false
                },
                {
                    targets :13,
                    className:'noClick align-middle hide',
                    visible: false
                },
                {
                    targets :14,
                    className:'noClick align-middle',
                    visible: false
                },
                {
                    targets :15,
                    className:'noClick align-middle',
                    visible: false
                },
                {
                    targets :16,
                    className:'noClick align-middle',
                    visible: false
                },
            ],
        });


        table.on('change', '.kt-group-checkable', function () {
            var set = $(this).closest('table').find('td:first-child .kt-checkable');
            var checked = $(this).is(':checked');

            $(set).each(function () {
                if (checked) {
                    $(this).prop('checked', true);
                    $(this).closest('tr').addClass('active');

                } else {
                    $(this).prop('checked', false);
                    $(this).closest('tr').removeClass('active');
                }
            });
        });

        table.on('change', 'tbody tr .kt-checkbox', function () {
            $(this).parents('tr').toggleClass('active');
            $(this).trigger("click");

        });

        table.on('click', 'tbody tr td', function () {
            var tr = $(this);
            if (tr.hasClass("noClick")) {
                return false;
            }
        });

    };

    return {

        //main function to initiate the module
        init: function () {
            initTableLeadCampaign();
        },

    };


}();

jQuery(document).ready(function () {
    KTDatatablesLeadCampaign.init();

    var table = $('#leads_per_campaing').DataTable();
    let rows = table.rows([ '#duplicate' ]);
    let arr_cols = table.columns()[0];
    let default_cols = 16;
    let action_col = 1 // Number of action columns
    let len_cols = arr_cols.length - action_col; // Last column has to be eliminated from Custom fields.
    let cus_field_cols = [];

    // Calculate the custom fields.
    if(len_cols > default_cols) {
        let diff_cols = len_cols - default_cols;
        for(let i = default_cols+1; i< len_cols; i++) {
            cus_field_cols.push(i);
        }
    }
    // Hide Custom field columns.
    table.columns(cus_field_cols).visible(false);

    //Search Lead Table
    $('#lead-search').keyup(function (e) {
        var value = $(this).val();
        // Search value only on 'Enter' is pressed
        if (e.keyCode == 13 && value.length >= 3) {
            table
                .search(value)
                .draw();
        } else {
            table
                .search('')
                .draw();
        }

    });

    // Filter By Campaign
    $('#filter_by_campaign').change(function (e) {
        var value = $(this).children("option:selected").val();
        var regex = '\\b' + value + '\\b';
        table
            .columns(1)
            .search(regex, true, false)
            .draw();
    });


    // Custom field Checkbox
    $('input[name="cb-cus-fields"]').on('change',  function() {
        if($(this).is('.ac-sel')) {
            $('input#cb-cus-fields').removeClass('ac-sel');
            table.columns(cus_field_cols).visible( false );
            table.draw();
        }else {
            $('input#cb-cus-fields').addClass('ac-sel');
            table.columns(cus_field_cols).visible( true );
            table.draw();
        }
    });

    // Traffic source Checkbox
    $('input[name="cb-ts"]').on('change',  function() {
        if($(this).is('.ac-sel')) {
            $('input#cb-ts').removeClass('ac-sel');
            table.columns( [9] ).visible( false );
        }else {
            $(this).addClass("ac-sel");
            table.columns( [9] ).visible( true );
        }
        table.columns.adjust().draw(  );
    });

    // UTM Checkbox
    $('input[name="cb-utm-parms"]').on('change',  function() {
        if($(this).is('.ac-sel')) {
            $(this).removeClass('ac-sel');
            table.columns( [10] ).visible( false );
        }else {
            $(this).addClass('ac-sel');
            table.columns( [10] ).visible( true );
        }
        table.columns.adjust().draw(  );
    });

    // TargetGroup Checkbox
    $('input[name="cb-tg"]').on('change',  function() {
        if($(this).is('.ac-sel')) {
            $(this).removeClass('ac-sel');
            table.columns( [11] ).visible( false );
        }else {
            $(this).addClass('ac-sel');
            table.columns( [11] ).visible( true );
        }
        table.columns.adjust().draw(  );
    });

    // affiliate Id's Checkbox
    $('input[name="cb-aff"]').on('change',  function() {
        if($(this).is('.ac-aff')) {
            $(this).removeClass('ac-aff');
            table.columns( [12,13] ).visible( false );
            table.columns.adjust().draw();
        }else {
            $(this).addClass('ac-aff');
            table.columns( [12,13] ).visible( true );
            table.columns.adjust().draw();
        }
    });

    // Filter By Status
    $('#filter_by_status').change(function (e) {
        var value = $(this).children("option:selected").val();
        var regex = '\\b' + value + '\\b';
        table
            .columns(8)
            .search(regex, true, false)
            .draw();


    });

    // Date Range Picker
    $('input[name="daterange"]').daterangepicker({
        timePicker: false,
        autoApply: false,
        autoUpdateInput: true,
        alwaysShowCalendars: false,

        locale: {
            format: 'Y-MM-D',
            cancelLabel: 'Clear'
        },

        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }

    });

    // Search Datatable with selected Date range
    $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
        var d_range = picker.startDate.format('Y-MM-D') + ' - ' + picker.endDate.format('Y-MM-D')
        $(this).val(d_range);
        var d_arr = getDiffDates(d_range);
        var searchString = '(' + d_arr.join('|') + ')';
        table.columns(3).search(searchString,true).draw(true);
    });

    // Reset the Date range filter
    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        table
            .search('')
            .draw();
    });


    // Get all the dates between Min and Max Date.
    function getDiffDates(daterange) {
        var dates = [];
        var split_range = daterange.split(" - ");
        var from_date = new Date(split_range[0]);
        var to_date = new Date(split_range[1]);
        var current_date = from_date;

        while (current_date <= to_date) {
            var date = new Date(current_date);
            var m = (date.getMonth() + 1).toString().replace(/(^.$)/, "0$1");
            var y = date.getFullYear();
            var d = (date.getDate() < 10 ? '0' : '') + date.getDate()
            var f_date = y + '-' + m + '-' + d;
            dates.push((f_date));
            current_date.setDate(current_date.getDate() + 1);
        }
        return dates;
    }

    // Appends Export Button to datatable.
    var buttons = new $.fn.dataTable.Buttons(table, {
        buttons: [
            'excelHtml5',
        ]
    }).container().appendTo($('#export-table'));

    // Hide the Custom fields by default.
/*    $( '#show-cus-fields' ).trigger( "click" );*/

});
