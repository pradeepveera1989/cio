<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;


class CompanyTest extends TestCase {
    
    public function setUp() {
        
        $this->company = new App\Models\Company($db);
    }        
        
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid arguments passed to update Company
     */     
    public function testUpdateCompanyById(){
        
        $this->company->updateCompanyById($p = array());
    }
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid arguments passed to update Company
     */     
    public function testUpdateCompanyByIdWithNoId(){
        
        $p = [
            'email' => 'test@email.com',
            'id'    => ''
        ];
        
        $this->company->updateCompanyById($p);
    }    
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid Company id
     */       
    public function testGetCompanyDetailsById(){
        
        $this->company->getCompanyDetailsById("");
    }
    
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid Company id
     */       
    public function testGetCompanyDetailsByIdWithNumberCheck(){
        
        $this->company->getCompanyDetailsById("asdf");
    }     
}